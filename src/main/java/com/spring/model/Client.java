package com.spring.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="client")
public class Client  {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "client_id")
	private int client_id;
	
	@Column(name = "client_name")
	private String client_name;
	
	/*@Column(name = "location")
	private String location;*/
	
	@Column(name = "mail_id")
	private String mail_id;
	
	@Column(name = "domain")
	private String domain;
	
	@Column(name = "client_url")
	private String client_url;
	
	@Column(name = "is_active")
	private Short is_active;
	
	@Column(name = "row_created")
	private Date rowCreated;

	@Column(name = "row_altered")
	private Date rowAltered;
	
	@Column(name = "spoc")
	private String spoc;
	
	@OneToMany(mappedBy = "clientFk", cascade = CascadeType.PERSIST)
	private List<ClientLocation> clientLocation = new ArrayList<ClientLocation>();
	
	@OneToMany(mappedBy="clientFk",cascade=CascadeType.PERSIST)
	private List<Tracker> tracker = new ArrayList<Tracker>();
	
	
	public int getClient_id() {
		return client_id;
	}
	public void setClient_id(int client_id) {
		this.client_id = client_id;
	}
	public String getClient_name() {
		return client_name;
	}
	public void setClient_name(String client_name) {
		this.client_name = client_name;
	}
	/*public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}*/
	
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	
	
	public Short getIs_active() {
		return is_active;
	}
	public void setIs_active(Short is_active) {
		this.is_active = is_active;
	}
	public String getMail_id() {
		return mail_id;
	}
	public void setMail_id(String mail_id) {
		this.mail_id = mail_id;
	}
	public String getClient_url() {
		return client_url;
	}
	public void setClient_url(String client_url) {
		this.client_url = client_url;
	}
	public Date getRowCreated() {
		return rowCreated;
	}
	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}
	public Date getRowAltered() {
		return rowAltered;
	}
	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}
	public String getSpoc() {
		return spoc;
	}
	public void setSpoc(String spoc) {
		this.spoc = spoc;
	}
	public List<Tracker> getTracker() {
		return tracker;
	}
	public void setTracker(List<Tracker> tracker) {
		this.tracker = tracker;
	}
	
	public Client(){
		
	}
	public List<ClientLocation> getClientLocation() {
		return clientLocation;
	}
	public void setClientLocation(List<ClientLocation> clientLocation) {
		this.clientLocation = clientLocation;
	}
	
}
