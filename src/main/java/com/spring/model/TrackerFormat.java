package com.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="tracker_format")
public class TrackerFormat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "tracker_format_id")
	private Integer trackerFormatId;
	
	@Column(name = "field")
	private String field;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="tracker_fk_tracker_id")
	private Tracker trackerFk;
	
	@Column(name = "is_active")
	private Short is_active;
	
	@Column(name = "row_created")
	private Date rowCreated;

	@Column(name = "row_altered")
	private Date rowAltered;


	public Integer getTrackerFormatId() {
		return trackerFormatId;
	}

	public void setTrackerFormatId(Integer trackerFormatId) {
		this.trackerFormatId = trackerFormatId;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	

	public Tracker getTrackerFk() {
		return trackerFk;
	}

	public void setTrackerFk(Tracker trackerFk) {
		this.trackerFk = trackerFk;
	}

	public Short getIs_active() {
		return is_active;
	}

	public void setIs_active(Short is_active) {
		this.is_active = is_active;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}
	
	
	
}
