package com.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name="team_user")
public class TeamUser {

	@Id
	@Column(name="team_user_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int teamUserId;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="user_fk_user_rowid")
	private User userFk;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="team_fk_team_id")
	private Team teamFk;
	
	@Column(name="is_active")
	private short isActive;
	
	@Column(name="row_created")
	private Date rowCreated;
	
	@Column(name="row_altered")
	private Date rowAltered;

	public int getTeamUserId() {
		return teamUserId;
	}

	public void setTeamUserId(int teamUserId) {
		this.teamUserId = teamUserId;
	}

	public User getUserFk() {
		return userFk;
	}

	public void setUserFk(User userFk) {
		this.userFk = userFk;
	}

	public Team getTeamFk() {
		return teamFk;
	}

	public void setTeamFk(Team teamFk) {
		this.teamFk = teamFk;
	}

	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isActive) {
		this.isActive = isActive;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}
	
	
	
	
}
