package com.spring.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="team")
public class Team {

	@Id
	@Column(name="team_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int teamId;
	
	@Column(name="team_name")
	private String teamName;
	
	@Column(name="is_active")
	private short isActive;
	
	@Column(name="row_created")
	private Date rowCreated;
	
	@Column(name="row_altered")
	private Date rowAltered;
	
	@ManyToOne
	@JoinColumn(name="user_fk_user_rowid")
	private User userFk;

	@OneToMany(mappedBy = "teamFk", cascade = CascadeType.PERSIST)
	private List<TeamUser> teamUser = new ArrayList<TeamUser>();
	
	
	public int getTeamId() {
		return teamId;
	}

	public void setTeamId(int teamId) {
		this.teamId = teamId;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isActive) {
		this.isActive = isActive;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}

	public List<TeamUser> getTeamUser() {
		return teamUser;
	}

	public void setTeamUser(List<TeamUser> teamUser) {
		this.teamUser = teamUser;
	}

	public User getUserFk() {
		return userFk;
	}

	public void setUserFk(User userFk) {
		this.userFk = userFk;
	}
	
	
	
	
}
