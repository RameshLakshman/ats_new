package com.spring.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tracker")
public class Tracker implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "tracker_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int trackerId;

	@Column(name = "location")
	private String location;
	
	@Column(name = "primary_skills")
	private String primarySkills;
	
	@Column(name = "additional_skills")
	private String additionalSkills;
	
	@Column(name = "experience_from")
	private int experienceFrom;
	
	@Column(name = "experience_to")
	private int experienceTo;
	
	@Column(name = "salary_from")
	private int salaryFrom;
	
	@Column(name = "salary_to")
	private int salaryTo;
	
	
	@Column(name="is_active")
	private short isActive;
	
	 @Column(name="row_created")
	private Date rowCreated;
	
	 @Column(name="row_altered")
	private Date rowAltered;
	 
    @NotNull
	@ManyToOne
	@JoinColumn(name="client_fk_client_id")
	private Client clientFk;
    
   // @NotNull
  	@ManyToOne
  	@JoinColumn(name="tf_namefk_tf_name_id")
  	private TrackerFormatName tfNamefk;
    
   // @NotNull
	@ManyToOne
	@JoinColumn(name="qusname_fk_questionnaire_id")
	private Questionnaire questionnaireFk;
	 
    @OneToMany(mappedBy="trackerFk",cascade=CascadeType.PERSIST)
	private List<TrackerFormat> trackerFormat = new ArrayList<TrackerFormat>();
    
    
    @OneToMany(mappedBy="trackerFk",cascade=CascadeType.PERSIST)
   	private List<TrackerQuestionnaire> trackerQuestionnaire = new ArrayList<TrackerQuestionnaire>();
    
    
	public int getTrackerId() {
		return trackerId;
	}

	public void setTrackerId(int trackerId) {
		this.trackerId = trackerId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getPrimarySkills() {
		return primarySkills;
	}

	public void setPrimarySkills(String primarySkills) {
		this.primarySkills = primarySkills;
	}

	public String getAdditionalSkills() {
		return additionalSkills;
	}

	public void setAdditionalSkills(String additionalSkills) {
		this.additionalSkills = additionalSkills;
	}

	public int getSalaryFrom() {
		return salaryFrom;
	}

	public void setSalaryFrom(int salaryFrom) {
		this.salaryFrom = salaryFrom;
	}

	public int getSalaryTo() {
		return salaryTo;
	}

	public void setSalaryTo(int salaryTo) {
		this.salaryTo = salaryTo;
	}

	public int getExperienceFrom() {
		return experienceFrom;
	}

	public void setExperienceFrom(int experienceFrom) {
		this.experienceFrom = experienceFrom;
	}

	public int getExperienceTo() {
		return experienceTo;
	}

	public void setExperienceTo(int experienceTo) {
		this.experienceTo = experienceTo;
	}

	
	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isActive) {
		this.isActive = isActive;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}

	public Client getClientFk() {
		return clientFk;
	}

	public void setClientFk(Client clientFk) {
		this.clientFk = clientFk;
	}

	public List<TrackerFormat> getTrackerFormat() {
		return trackerFormat;
	}

	public void setTrackerFormat(List<TrackerFormat> trackerFormat) {
		this.trackerFormat = trackerFormat;
	}

	public TrackerFormatName getTfNamefk() {
		return tfNamefk;
	}

	public void setTfNamefk(TrackerFormatName tfNamefk) {
		this.tfNamefk = tfNamefk;
	}

	public Questionnaire getQuestionnaireFk() {
		return questionnaireFk;
	}

	public void setQuestionnaireFk(Questionnaire questionnaireFk) {
		this.questionnaireFk = questionnaireFk;
	}

	public List<TrackerQuestionnaire> getTrackerQuestionnaire() {
		return trackerQuestionnaire;
	}

	public void setTrackerQuestionnaire(
			List<TrackerQuestionnaire> trackerQuestionnaire) {
		this.trackerQuestionnaire = trackerQuestionnaire;
	}


}
