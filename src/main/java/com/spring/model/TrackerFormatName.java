package com.spring.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="tracker_format_name")
public class TrackerFormatName {

	@Id
	@Column(name="tf_name_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	 @Column(name="tracker_format_name")
	private String trackerFormatName;
	
	 @Column(name="is_active")
	private short isActive;
	
	 @Column(name="row_created")
	private Date rowCreated;
	
	 @Column(name="row_altered")
	private Date rowAltered;
	 
	 
	 @OneToMany(mappedBy="tfNamefk",cascade=CascadeType.PERSIST)
		private List<Tracker> tracker = new ArrayList<Tracker>();
	 
	 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTrackerFormatName() {
		return trackerFormatName;
	}

	public void setTrackerFormatName(String trackerFormatName) {
		this.trackerFormatName = trackerFormatName;
	}

	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isActive) {
		this.isActive = isActive;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}

	public List<Tracker> getTracker() {
		return tracker;
	}

	public void setTracker(List<Tracker> tracker) {
		this.tracker = tracker;
	}
	
	 
	 
	 
}
