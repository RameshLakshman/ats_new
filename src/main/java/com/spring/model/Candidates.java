package com.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Ramesh
 *
 */
@Entity
@Table(name = "candidate")
public class Candidates { 
	@Id
	@Column(name = "candidate_id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int candidate_id;

	@NotNull
	@Column(name = "name")
	private String name;

	@NotNull
	@Column(name = "mail_id")
	private String mailId;

	@NotNull
	@Column(name = "mobile_number")
	private String mobileNumber;

	@NotNull
	@Column(name = "ctc")
	private String ctc;

	@NotNull
	@Column(name = "is_active")
	private Short is_active;

	@NotNull
	@Column(name = "row_created")
	private Date rowCreated;

	@Column(name = "row_altered")
	private Date rowAltered;

	@NotNull
	@Column(name = "notice_period")
	private String noticePeriod;

	@Column(name = "experience_from")
	private String experienceFrom;

	@Column(name = "experience_to")
	private String experienceTo;

	@Column(name = "prefered_job_location")
	private String preferedJobLocation;

	@Column(name = "resume_filepath_name")
	private String resumeFilepathName;

	@Column(name = "primary_skill")
	private String primarySkill;

	@Column(name = "current_company")
	private String currentCompany;

	

	public String getCandidateName() {
		return name;
	}

	public int getCandidate_id() {
		return candidate_id;
	}

	public void setCandidate_id(int candidate_id) {
		this.candidate_id = candidate_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}

	public void setCandidateName(String candidateName) {
		this.name = candidateName;
	}

	public String getMail_id() {
		return mailId;
	}

	public void setMail_id(String mail_id) {
		this.mailId = mail_id;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getCtc() {
		return ctc;
	}

	public void setCtc(String ctc) {
		this.ctc = ctc;
	}

	public Short getIs_active() {
		return is_active;
	}

	public void setIs_active(Short is_active) {
		this.is_active = is_active;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}

	public String getNoticePeriod() {
		return noticePeriod;
	}

	public void setNoticePeriod(String noticePeriod) {
		this.noticePeriod = noticePeriod;
	}

	public String getExperienceFrom() {
		return experienceFrom;
	}

	public void setExperienceFrom(String experienceFrom) {
		this.experienceFrom = experienceFrom;
	}

	public String getExperienceTo() {
		return experienceTo;
	}

	public void setExperienceTo(String experienceTo) {
		this.experienceTo = experienceTo;
	}

	public String getPreferedJobLocation() {
		return preferedJobLocation;
	}

	public void setPreferedJobLocation(String preferedJobLocation) {
		this.preferedJobLocation = preferedJobLocation;
	}

	public String getResumeFilepathName() {
		return resumeFilepathName;
	}

	public void setResumeFilepathName(String resumeFilepathName) {
		this.resumeFilepathName = resumeFilepathName;
	}

	public String getPrimarySkill() {
		return primarySkill;
	}

	public void setPrimarySkill(String primarySkill) {
		this.primarySkill = primarySkill;
	}

	public String getCurrentCompany() {
		return currentCompany;
	}

	public void setCurrentCompany(String currentCompany) {
		this.currentCompany = currentCompany;
	}

}
