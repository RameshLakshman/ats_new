package com.spring.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.internal.NotNull;

@Entity
@Table(name="client_location")
public class ClientLocation {
	@Id
	@Column(name="client_location_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int clientLocationId;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="client_fk_client_id")
	private Client clientFk;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="location_fk_location_id")
	private Location locationFk;
	
	@Column(name = "is_active")
	private Short is_active;
	
	@Column(name = "row_created")
	private Date rowCreated;

	@Column(name = "row_altered")
	private Date rowAltered;

	public int getClientLocationId() {
		return clientLocationId;
	}

	public void setClientLocationId(int clientLocationId) {
		this.clientLocationId = clientLocationId;
	}

	public Client getClientFk() {
		return clientFk;
	}

	public void setClientFk(Client clientFk) {
		this.clientFk = clientFk;
	}

	public Location getLocationFk() {
		return locationFk;
	}

	public void setLocationFk(Location locationFk) {
		this.locationFk = locationFk;
	}

	public Short getIs_active() {
		return is_active;
	}

	public void setIs_active(Short is_active) {
		this.is_active = is_active;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}




}
