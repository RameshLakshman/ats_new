package com.spring.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="location")
public class Location {

	@Id
	@Column(name="location_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int locationId;
	
	 @Column(name="location")
	private String location;
	
	 @Column(name="is_active")
	private short isActive;
	
	 @Column(name="row_created")
	private Date rowCreated;
	
	 @Column(name="row_altered")
	private Date rowAltered;
	
	@OneToMany(mappedBy = "locationFk", cascade = CascadeType.PERSIST)
	private List<ClientLocation> clientLocation = new ArrayList<ClientLocation>();

	public int getLocationId() {
		return locationId;
	}

	public void setLocationId(int locationId) {
		this.locationId = locationId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public short getIsActive() {
		return isActive;
	}

	public void setIsActive(short isActive) {
		this.isActive = isActive;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}

	public List<ClientLocation> getClientLocation() {
		return clientLocation;
	}

	public void setClientLocation(List<ClientLocation> clientLocation) {
		this.clientLocation = clientLocation;
	}


}
