package com.spring.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="questionnaire")
public class Questionnaire{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "questionnaire_id")
	private Integer questionnaireId;
	
	@Column(name = "questionnaire_name")
	private String questionnaireName;
	
	
	@Column(name = "is_active")
	private Short isActive;
	
	@Column(name = "row_created")
	private Date rowCreated;

	@Column(name = "row_altered")
	private Date rowAltered;
	
	
	@OneToMany(mappedBy="questionnaireFk",cascade=CascadeType.PERSIST)
	private List<Tracker> trackerQus = new ArrayList<Tracker>();

	
	public Integer getQuestionnaireId() {
		return questionnaireId;
	}

	public void setQuestionnaireId(Integer questionnaireId) {
		this.questionnaireId = questionnaireId;
	}

	public String getQuestionnaireName() {
		return questionnaireName;
	}

	public void setQuestionnaireName(String questionnaireName) {
		this.questionnaireName = questionnaireName;
	}

	public Short getIsActive() {
		return isActive;
	}

	public void setIsActive(Short isActive) {
		this.isActive = isActive;
	}

	public Date getRowCreated() {
		return rowCreated;
	}

	public void setRowCreated(Date rowCreated) {
		this.rowCreated = rowCreated;
	}

	public Date getRowAltered() {
		return rowAltered;
	}

	public void setRowAltered(Date rowAltered) {
		this.rowAltered = rowAltered;
	}

	
	public List<Tracker> getTrackerQus() {
		return trackerQus;
	}

	public void setTrackerQus(List<Tracker> trackerQus) {
		this.trackerQus = trackerQus;
	}

	

}
