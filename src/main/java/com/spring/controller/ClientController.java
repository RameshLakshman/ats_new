package com.spring.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.dao.ClientDao;
import com.spring.model.Client;

@Controller
public class ClientController {

	@Autowired
	ClientDao clientDao;
	
	@RequestMapping(value="/clientList",method = RequestMethod.GET)
	public String home(Model model){
		try {
		List<Client> allClients = clientDao.getAllClients();
		model.addAttribute("allClients",allClients);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "ManageClient/list";
	}
	
	@RequestMapping(value="/addclient")
	public String addClient(){
		
		return "ManageClient/create";
	}
	
	@RequestMapping(value="/saveClient",method = RequestMethod.POST)
	public String saveClient(@RequestParam("client_name") String client_name,
			@RequestParam("domain") String domain,
			@RequestParam("mail_id") String mail_id,
			@RequestParam("client_url") String client_url,@RequestParam("spoc") String spoc,
			@RequestParam("is_active") Short is_active,RedirectAttributes redirectAttr){
		try {
		Client client = new Client();
		//client.setClient_id(3);
		client.setClient_name(client_name);
		client.setDomain(domain);
		client.setClient_url(client_url);
		//client.setLocation(location);
		client.setMail_id(mail_id);
		client.setIs_active(is_active);
		client.setRowCreated(new Date());
		client.setSpoc(spoc);
		
		clientDao.addClient(client);
		redirectAttr.addFlashAttribute("message", "Client has been added successfully");
	} catch (Exception e) {
		System.out.println(e);
	}
		return "redirect:clientList";
	}
	
	@RequestMapping(value="/editClient")
	public String editClient(Model model,@RequestParam("client_id") Integer client_id){
		try {
		 Client client = clientDao.getClientById(client_id);
		 model.addAttribute("client", client);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "ManageClient/edit";
	}
	
	@RequestMapping(value="/updateClient")
	public String updateClient(Model model,@RequestParam("client_id") Integer client_id,
			@RequestParam("client_name") String client_name,
			@RequestParam("domain") String domain,
			@RequestParam("mail_id") String mail_id,
			@RequestParam("client_url") String client_url,@RequestParam("spoc") String spoc,
			@RequestParam("is_active") Short is_active,RedirectAttributes redirectAttr){
		try {
		Client client = clientDao.getClientById(client_id);
		client.setClient_id(client_id);
		client.setClient_name(client_name);
		client.setDomain(domain);
		client.setClient_url(client_url);
		//client.setLocation(location);
		client.setMail_id(mail_id);
		client.setIs_active(is_active);
		//client.setRowCreated(new Date());
		client.setRowAltered(new Date());
		client.setSpoc(spoc);
		
		clientDao.updateClient(client);
		redirectAttr.addFlashAttribute("message", "Client has been updated successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return "redirect:clientList";
	}
	
	@RequestMapping(value="/deleteClient")
	public String deleteClient(@RequestParam("client_id") Integer client_id,RedirectAttributes redirectAttr){
		try {
		 clientDao.removeClient(client_id);
		 redirectAttr.addFlashAttribute("message", "Client has been deleted successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
		
		return "redirect:clientList";
	}
}
