package com.spring.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import com.spring.dao.CandidateDAO;
import com.spring.model.Candidates;


@RestController
public class CandidateController {

	
	
	@Autowired
	CandidateDAO candidateDao;
	
	
	/**
	 * @param model
	 * @return
	 * Front side candidate list
	 */
	@RequestMapping(value="/produceCandidateList",method=RequestMethod.GET,headers = "Accept=application/json")
	public List<Candidates> produceCandidateList()
	{
		List<Candidates> candidate=null;
		try
		{
		System.out.println("candidateList #######################");
		 candidate=candidateDao.candidateList();
		String checkBox = "";
		// checkBox = "<input type='checkbox' name='utilisateurModif' property='choixUtilisateur'/>";
		//model.addAttribute("candidateList", candidate);
		//model.addAttribute("checkBox", checkBox);
		
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
		return candidate;
		 
	}
	
	
	//Consume Candidate List
	@RequestMapping(value="/candidateList",method = RequestMethod.GET)
	public ModelAndView candidateList(){
		System.out.println("inside;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;");
		
		
		 ArrayList jo=null;
		ModelAndView mav =null;
		try
		{
		URL url = new URL("http://localhost:8080/ATS/produceCandidateList");
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");
		
		//response Check
		if (conn.getResponseCode() != 200) {
			System.out.println("ok Response;;;;;;;;;;;;;;;;;");
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}
		
		
		BufferedReader br = new BufferedReader(new InputStreamReader(
				(conn.getInputStream())));

			String output;
			StringBuffer buffer = new StringBuffer();
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				buffer.append(output);
				System.out.println(output);
			}
			System.out.println("output ^^^^^^^^^^^^^^^^^^^^"+buffer.toString());
			JSONArray jsa = new JSONArray(buffer.toString());
			System.out.println("jsa;;;;;;JSONObject;;;;;;;;;;;;;;;;;;;"+jsa);			
			 
			
			List<Candidates> listStrings = new ArrayList<Candidates>();
			ArrayList<String> candidateList1 = new ArrayList<String>();     
			JSONArray jArray = (JSONArray)jsa; 
			if (jArray != null) { 
			   for (int i=0;i<jArray.length();i++){
				   Candidates candidates = new Candidates();
				   JSONObject object = jArray.optJSONObject(i);
				   candidates.setCandidate_id(object.optInt("candidate_id"));
				   candidates.setName(object.optString("name"));
				   candidates.setMail_id(object.optString("mailId"));
				   candidates.setMobileNumber(object.optString("mobileNumber"));
				   candidates.setCurrentCompany(object.optString("currentCompany"));
				   candidates.setCtc(object.optString("ctc"));
				  candidates.setNoticePeriod(object.optString("noticePeriod"));
				   candidates.setResumeFilepathName(object.optString("resumeFilepathName"));
				  // candidates.setIs_active(object.optInt(("is_active"));
				 //  candidates.setRowCreated(object.optString("rowCreated"));
				   listStrings.add(candidates);
			   } 
			  
			} 
			
			
			ModelAndView modelAndView =  new ModelAndView("Candidate/candidateList");
			modelAndView.addObject("candidateList", listStrings);
			System.out.println("candidateList;;{}{}{}{}{}{};;;;;;;;;;;;"+candidateList1);
			/*mav.addObject("candidateList1", candidateList1);
			mav = new ModelAndView("Candidate/candidateList");*/
		    return modelAndView;
		}
		catch(Exception exception)
		{
		System.out.println(exception);
		}
		return mav;
	}	
		
	
	
	@RequestMapping(value="/createCandidate",method = RequestMethod.GET)
	public String signUp(Model model){
		model.addAttribute("candidates", new Candidates());
		return "Candidate/createCandidate";
	}	
		

	/*@RequestMapping(value="/dowloadCandidateDetails",method = RequestMethod.POST)
	public @ResponseBodyString dowloadCandidateDetails(@RequestParam(value="myArray[]") Integer[] myArray){
		System.out.println("dowloadCandidateDetails"); 
		return null;
		
		
	}		*/
	
}
