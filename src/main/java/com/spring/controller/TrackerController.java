package com.spring.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.dao.ClientDao;
import com.spring.dao.TrackerDao;
import com.spring.model.Client;
import com.spring.model.Location;
import com.spring.model.Questionnaire;
import com.spring.model.Tracker;
import com.spring.model.TrackerFormat;
import com.spring.model.TrackerFormatName;
import com.spring.model.TrackerQuestionnaire;
import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;


@Controller
public class TrackerController {
	
	@Autowired
	TrackerDao trackerDao;

	@Autowired
	ClientDao clientDao;
	
	@RequestMapping(value="/addTracker",method = RequestMethod.GET)
	public String create(Model model){
		List<Client> clientList = clientDao.getAllClients();
		model.addAttribute("allClients", clientList);
		return "ManageTracker/create";
	}
	
	@RequestMapping(value="/viewTracker",method = RequestMethod.GET)
	public String view(Model model){
		List<Tracker> trackerList = trackerDao.getAllTrackers();
		model.addAttribute("allTrackers", trackerList);
		return "ManageTracker/view"; 
	}

	@RequestMapping(value="/trackers",method = RequestMethod.GET)
	public String trackers(Model model){
		List<Client> clientList = clientDao.getAllClients();
		model.addAttribute("allClients", clientList);
		List<Location> locationList = trackerDao.getLocations();
		model.addAttribute("locationList", locationList);
		List<Tracker> trackerList = trackerDao.getAllTrackers();
		model.addAttribute("allTrackers", trackerList);
		
		Map<TrackerFormatName, ArrayList<TrackerFormat>> prevFormats = getPreviousFormats();
		Map<Questionnaire, ArrayList<TrackerQuestionnaire>> prevQuestions = getPreviousQuestions();
		model.addAttribute("prevFormats", prevFormats);
		model.addAttribute("prevQuestions", prevQuestions);
		
		return "trackers";
	}
	
	@RequestMapping(value="/addTracker",method = RequestMethod.POST)
	public String addTracker(Model model,
			@RequestParam("existsClient") String existsClient,
			@RequestParam("primarySkill") String primarySkill,
			@RequestParam("location") String location,
			@RequestParam("additionalSkill") String additionalSkill,
			@RequestParam("experienceFrom") int experienceFrom,
			@RequestParam("experienceTo") int experienceTo,
			@RequestParam("salaryFrom") int salaryFrom,
			@RequestParam("salaryTo") int salaryTo){
		
			Integer clientId = Integer.parseInt(existsClient);
			Client clientFk = clientDao.getClientById(clientId);
			
			Tracker tracker = new Tracker();
			tracker.setPrimarySkills(primarySkill);
			tracker.setAdditionalSkills(additionalSkill);
			tracker.setLocation(location);
			tracker.setExperienceFrom(experienceFrom);
			tracker.setExperienceTo(experienceTo);
			tracker.setSalaryFrom(salaryFrom);
			tracker.setSalaryTo(salaryTo);
			tracker.setRowCreated(new Date());
			tracker.setClientFk(clientFk);
			trackerDao.save(tracker);
			return "redirect:/viewTracker";
	}
	
	
	@RequestMapping(value="/createTracker",method = RequestMethod.POST)
	@ResponseBody
	public Integer add1Tracker(Model model,
			@RequestParam("existsClient") String existsClient,
			@RequestParam("primarySkill") String primarySkill,
			@RequestParam("additionalSkill") String additionalSkill,
			@RequestParam("experienceFrom") int experienceFrom,
			@RequestParam("experienceTo") int experienceTo,
			@RequestParam("jobLocation") String jobLocation,
			@RequestParam("salaryFrom") int salaryFrom,
			@RequestParam("salaryTo") int salaryTo){
		
			Integer clientId = Integer.parseInt(existsClient);
			Client clientFk = clientDao.getClientById(clientId);
			
			Tracker tracker = new Tracker();
			tracker.setPrimarySkills(primarySkill);
			tracker.setAdditionalSkills(additionalSkill);
			tracker.setExperienceFrom(experienceFrom);
			tracker.setExperienceTo(experienceTo);
			tracker.setLocation(jobLocation);
			tracker.setSalaryFrom(salaryFrom);
			tracker.setSalaryTo(salaryTo);
			tracker.setIsActive((short) 1);
			tracker.setRowCreated(new Date());
			tracker.setClientFk(clientFk);
		    Integer trackerId = trackerDao.save(tracker);
		    //System.out.println("FINALLY:::: "+trackerId);
			return trackerId;
	}
	
	
	
	
	@RequestMapping(value="/editTracker",method = RequestMethod.GET)
	public String edit(Model model,@RequestParam("tracker_id") int trackerId){
		
		Tracker getTracker = trackerDao.getTracker(trackerId);
		Client getClient = clientDao.getClientById(getTracker.getClientFk().getClient_id());
		List<Client> clientList = clientDao.getAllClients();
		model.addAttribute("allClients", clientList);
		model.addAttribute("tracker", getTracker);
		model.addAttribute("client", getClient);
		return "ManageTracker/edit";
	}
	@RequestMapping(value="/editTracker",method = RequestMethod.POST)
	public String update(Model model,@RequestParam("tracker_id") int trackerId,@RequestParam("existsClient") String existsClient,
			@RequestParam("primarySkill") String primarySkill,
			@RequestParam("location") String location,
			@RequestParam("additionalSkill") String additionalSkill,
			@RequestParam("experienceFrom") int experienceFrom,
			@RequestParam("experienceTo") int experienceTo,
			@RequestParam("salaryFrom") int salaryFrom,
			@RequestParam("salaryTo") int salaryTo){
			
			Integer clientId = Integer.parseInt(existsClient);
			Client clientFk = clientDao.getClientById(clientId);
			
			Tracker tracker = trackerDao.getTracker(trackerId);
			tracker.setPrimarySkills(primarySkill);
			tracker.setAdditionalSkills(additionalSkill);
			tracker.setLocation(location);
			tracker.setExperienceFrom(experienceFrom);
			tracker.setExperienceTo(experienceTo);
			tracker.setSalaryFrom(salaryFrom);
			tracker.setSalaryTo(salaryTo);
			tracker.setRowCreated(new Date());
			tracker.setClientFk(clientFk);
			trackerDao.updateTracker(tracker);
			return "redirect:/viewTracker";
	}
	
	@RequestMapping(value="/deleteTracker",method = RequestMethod.GET)
	public String delete(Model model,@RequestParam("tracker_id") int trackerId){
		trackerDao.deleteTracker(trackerId);
		return "redirect:/viewTracker";
	}
	
	
	@RequestMapping(value="/searchTracker",method = RequestMethod.POST)
	@ResponseBody
	public Map<Integer, List<Tracker>> searchTracker(@RequestParam("client") String client,
			@RequestParam("expFrom") int expFrom,
			@RequestParam("expTo") int expTo,
			@RequestParam("skill") String skill){
        List<Tracker> searchList=new ArrayList<Tracker>();
        List<Tracker> trakList=new ArrayList<Tracker>();

		List<Client> clt =  clientDao.getClientByName(client);
		if(clt!=null && clt.size()>0){
			for(Client clients:clt){  
			     int id = clients.getClient_id();
			     String name = clients.getClient_name();
			     searchList=trackerDao.searchTrackers(id, expFrom, expTo, skill);
			     for(int i=0;i<searchList.size();i++){
			    	 trakList.add(getTracker(searchList.get(i),name));
			    	 
			    	
			     }
			   }   
		}
		System.out.println("trakList"+trakList);
		
		Map<Integer, List<Tracker>> y = new HashMap<Integer, List<Tracker>>();
		y.put(expTo, trakList);
		
		return y;
	}
	
	public Tracker getTracker(Tracker getTracker,String name){
		Tracker tracker = new Tracker();
		tracker.setTrackerId(getTracker.getTrackerId());
		tracker.setAdditionalSkills(getTracker.getAdditionalSkills());
		tracker.setExperienceFrom(getTracker.getExperienceFrom());
		tracker.setExperienceTo(getTracker.getExperienceTo());
		tracker.setPrimarySkills(getTracker.getPrimarySkills());
		tracker.setSalaryFrom(getTracker.getSalaryFrom());
		tracker.setSalaryTo(getTracker.getSalaryTo());
		tracker.setLocation(getTracker.getLocation());
		Client client = new Client();
		client.setClient_name(name);
		tracker.setClientFk(client);
		
	return tracker;
	}
	
	@RequestMapping(value="/newTrackerFormat",method = RequestMethod.POST)
	@ResponseBody
	public String newTrackerFormat(@RequestParam("trackerId") String trackerId, @RequestParam("tfname") String tfname,@RequestParam("field[]") String[] fields){
		String returnString = "";
		if(tfname !=null && tfname!=""){
			TrackerFormatName findtfName = trackerDao.findTrackerFormatName(tfname);
			if(findtfName == null){
				//save tracker format name (master table)
				TrackerFormatName insert = new TrackerFormatName();
				insert.setTrackerFormatName(tfname);
				insert.setIsActive((short)1);
				insert.setRowCreated(new Date());
				TrackerFormatName trackerFormatFk =  trackerDao.saveTrackerFormatName(insert);
				
				// get tracker by id
				Integer tId = Integer.parseInt(trackerId);
				Tracker tracker = trackerDao.getTracker(tId);
				tracker.setTfNamefk(trackerFormatFk);
				trackerDao.updateTracker(tracker);
				
				// save tracker format fields
				System.out.println(fields.length);
				for(int i=0;i<fields.length;i++){
					System.out.println(fields[i]);
					TrackerFormat tFormat = new TrackerFormat();
					tFormat.setField(fields[i]);
					tFormat.setTrackerFk(tracker);
					tFormat.setIs_active((short)1);
					tFormat.setRowCreated(new Date());
					trackerDao.saveTrackerFormat(tFormat);
					returnString = "true";
				}
			}else{
				returnString = "Tracker Format Name Already Exist";
			}
		}
		return returnString;
	}
	
	@RequestMapping(value = "/saveprevformat", method = RequestMethod.POST)
	@ResponseBody
	public String saveprevformat(@RequestParam("trackerId") String trackerId, @RequestParam("tfnameId") String tfnameId) {
		String returnPage="false";
			Integer tfnId = Integer.parseInt(tfnameId);
			Integer tId = Integer.parseInt(trackerId);
				if(tId !=null && tId !=0 && tfnId !=null && tfnId!=0)
				{
					TrackerFormatName tfnFk = trackerDao.trackerFormatNameById(tfnId);
					Tracker updateTracker = trackerDao.getTracker(tId);
					updateTracker.setTfNamefk(tfnFk);
					updateTracker.setRowAltered(new Date());
					trackerDao.updateTracker(updateTracker);
					Tracker tracker = trackerDao.trackerBytfnFk(tfnId);
					List<TrackerFormat> tfList = trackerDao.TrackerFormatByTracker(tracker.getTrackerId());
					if (tfList != null && tfList.size() > 0) {
						for (int i = 0; i < tfList.size(); i++) {
							Integer tfId = tfList.get(i).getTrackerFk().getTrackerId();
							TrackerFormat tFormat = new TrackerFormat();
								tFormat.setField(tfList.get(i).getField());
								tFormat.setTrackerFk(updateTracker);
								tFormat.setIs_active((short) 1);
								tFormat.setRowCreated(new Date());
								trackerDao.saveTrackerFormat(tFormat);
						}
					}	
					returnPage="true";
				}else returnPage="false";
			
				return returnPage;
	}
	
	@RequestMapping(value="/savenewques",method = RequestMethod.POST)
	@ResponseBody
	public String savenewques(@RequestParam("trackerId") String trackerId, @RequestParam("questionname") String questionname, @RequestParam("field[]") String[] fields){
		
		String returnString = "";
		
		if(questionname !=null && questionname!=""){
			Questionnaire findqusName = trackerDao.findQuestionnaireName(questionname);
			if(findqusName == null){
				//save tracker question name (master table)
				Questionnaire insert = new Questionnaire();
				insert.setQuestionnaireName(questionname);
				insert.setIsActive((short)1);
				insert.setRowCreated(new Date());
				Questionnaire trackerFormatFk =  trackerDao.saveQuestionnaireName(insert);
				
				// get tracker by id
				Integer tId = Integer.parseInt(trackerId);
				Tracker tracker = trackerDao.getTracker(tId);
				tracker.setQuestionnaireFk(trackerFormatFk);
				trackerDao.updateTracker(tracker);
				
				// save tracker format fields
			
				for(int i=0;i<fields.length;i++){
					TrackerQuestionnaire tqus = new TrackerQuestionnaire();
					tqus.setField(fields[i]);
					tqus.setTrackerFk(tracker);
					tqus.setIs_active((short)1);
					tqus.setRowCreated(new Date());
					trackerDao.saveTrackerQuestionnaire(tqus);
					returnString = "true";
				}
			}else{
				returnString = "Tracker Question Name Already Exist";
			}
		}
		return returnString;
	}
	
	@RequestMapping(value = "/saveprevquestions", method = RequestMethod.POST)
	@ResponseBody
	public String saveprevquestions(@RequestParam("trackerId") String trackerId, @RequestParam("questionNameId") String questionNameId) {
		String returnPage="false";
			Integer tqnId = Integer.parseInt(questionNameId);
			Integer tId = Integer.parseInt(trackerId);
				if(tId !=null && tId !=0 && tqnId !=null && tqnId!=0)
				{
					Questionnaire tfnFk = trackerDao.questionnaireById(tqnId);
					Tracker updateTracker = trackerDao.getTracker(tId);
					updateTracker.setQuestionnaireFk(tfnFk);
					updateTracker.setRowAltered(new Date());
					trackerDao.updateTracker(updateTracker);
					Tracker tracker = trackerDao.trackerByquestionnaireFk(tqnId);
					List<TrackerQuestionnaire> tfList = trackerDao.trackerQuestionnaireByTracker(tracker.getTrackerId());
					if (tfList != null && tfList.size() > 0) {
						for (int i = 0; i < tfList.size(); i++) {
							Integer tqId = tfList.get(i).getTrackerFk().getTrackerId();
							TrackerQuestionnaire tQus = new TrackerQuestionnaire();
							tQus.setField(tfList.get(i).getField());
								tQus.setTrackerFk(updateTracker);
								tQus.setIs_active((short) 1);
								tQus.setRowCreated(new Date());
								trackerDao.saveTrackerQuestionnaire(tQus);
						}
					}	
					returnPage="true";
				}else returnPage="false";
			
				return returnPage;
	}
	
	private Map<TrackerFormatName, ArrayList<TrackerFormat>> getPreviousFormats() {

		Map<TrackerFormatName, ArrayList<TrackerFormat>> map = null;
		try {
			map = new LinkedHashMap<TrackerFormatName, ArrayList<TrackerFormat>>();
			List<TrackerFormatName> TFNameList = trackerDao.getTFNameList();
			if (TFNameList != null && TFNameList.size() > 0) {
				for (int i = 0; i < TFNameList.size(); i++) {
					ArrayList<TrackerFormat> trackerFormat = new ArrayList<TrackerFormat>();
					TrackerFormatName tfnBean = TFNameList.get(i);
					int TFNameId = TFNameList.get(i).getId();
					
					Tracker tracker = trackerDao.trackerBytfnFk(TFNameId);
					Integer trackerId = tracker.getTrackerId();
					List<TrackerFormat> tfList = trackerDao.TrackerFormatByTracker(trackerId);
					if (tfList != null && tfList.size() > 0) {
						for (int j = 0; j < tfList.size(); j++) {
							TrackerFormat tfBean = tfList.get(j);
							trackerFormat.add(tfBean);
						}
					}
					map.put(tfnBean, trackerFormat);
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return map;
	}
	private Map<Questionnaire, ArrayList<TrackerQuestionnaire>> getPreviousQuestions() {
		Map<Questionnaire, ArrayList<TrackerQuestionnaire>> map = null;
		try {
			map = new LinkedHashMap<Questionnaire, ArrayList<TrackerQuestionnaire>>();
			List<Questionnaire> questionsList = trackerDao.getTQuestionList();
			if (questionsList != null && questionsList.size() > 0) {
				for (int i = 0; i < questionsList.size(); i++) {
					ArrayList<TrackerQuestionnaire> trackerFormat = new ArrayList<TrackerQuestionnaire>();
					Questionnaire questionnaireBean = questionsList.get(i);
					int quesId = questionsList.get(i).getQuestionnaireId();
					System.out.println("quesId:::::::" + quesId);
					Tracker tracker = trackerDao.trackerByquestionnaireFk(quesId);
					System.out.println("::::::::::" + tracker);
					Integer trackerId = tracker.getTrackerId();
					List<TrackerQuestionnaire> tqList = trackerDao.trackerQuestionnaireByTracker(trackerId);
					System.out.println("****************** " +tqList );
					if (tqList != null && tqList.size() > 0) {
						for (int j = 0; j < tqList.size(); j++) {
							TrackerQuestionnaire tqBean = tqList.get(j);
							trackerFormat.add(tqBean);
						}
					}
					map.put(questionnaireBean, trackerFormat);
				}
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return map;
	}
}
