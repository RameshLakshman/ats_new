package com.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	
	@RequestMapping(value="/",method = RequestMethod.GET)
	public String adminlogin(Model model){
		//model.addAttribute("allEmployees",employeeDao.getAllEmployees());
		return "login";
		//return "redirect:/trackers";
	}
	@RequestMapping(value="/adminPage",method = RequestMethod.GET)
	public String home(Model model){
		
		return "adminPage";
	}
}
