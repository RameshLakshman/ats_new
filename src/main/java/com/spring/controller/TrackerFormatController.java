package com.spring.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.dao.TrackerDao;
import com.spring.dao.TrackerFormatDao;
import com.spring.model.Tracker;
import com.spring.model.TrackerFormat;
import com.spring.model.TrackerQuestionnaire;
@Controller
public class TrackerFormatController {
	@Autowired
	TrackerFormatDao trackerFormatDao;
	@Autowired
	TrackerDao trackerDao;
	
	
	@RequestMapping(value="/viewTrackerFormat",method = RequestMethod.GET)
	public String view(Model model){
		List<TrackerFormat> trackerFormatList = trackerFormatDao.viewTrackerFormat();
		System.out.println(":::::::::::::: "+trackerFormatList);
		model.addAttribute("allTrackerFormats", trackerFormatList);
		return "ManageTrackerFormat/view"; 
	}
	
	@RequestMapping(value="/trackerFormat",method = RequestMethod.GET)
	public String trackerFormat(Model model,@RequestParam("tracker_id") int trackerId){
		List<TrackerFormat> trackerFormatList = trackerFormatDao.getByTracker(trackerId);
		model.addAttribute("allTrackerFormats", trackerFormatList);
		return "ManageTrackerFormat/view";
	}
	
	@RequestMapping(value="/editrackerFormat")
	public String editrackerFormat(Model model,@RequestParam("tracker_format_id") Integer trackerFormatId){
		try {
		TrackerFormat trackerFormat = trackerFormatDao.getTrackerFormat(trackerFormatId);
		model.addAttribute("trackerFormat", trackerFormat);
		
		List<Tracker> trackerList = trackerDao.getAllTrackers();
		model.addAttribute("trackerList", trackerList);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "ManageTrackerFormat/edit";
	}
	
	@RequestMapping(value="/updateTrackerFormat")
	public String updateQues(Model model,@RequestParam("trackerFormatId") Integer trackerFormatId,
			@RequestParam("trackerFk") Integer tracker_id,
			@RequestParam("field") String field,
			@RequestParam("is_active") Short is_active,RedirectAttributes redirectAttr){
		try {
		Tracker tracker = trackerDao.getTracker(tracker_id);
		
		TrackerFormat updateTrackerFormat = trackerFormatDao.getTrackerFormat(trackerFormatId);
		updateTrackerFormat.setTrackerFk(tracker);
		updateTrackerFormat.setField(field);
		updateTrackerFormat.setIs_active(is_active);
		updateTrackerFormat.setRowAltered(new Date());
		trackerFormatDao.update(updateTrackerFormat);
		redirectAttr.addFlashAttribute("message", "TrackerFormat has been updated successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:viewTrackerFormat";
	}
	
}
