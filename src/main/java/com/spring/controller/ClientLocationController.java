package com.spring.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.dao.ClientDao;
import com.spring.dao.ClientLocationDao;
import com.spring.model.Client;
import com.spring.model.ClientLocation;
import com.spring.model.Location;

@Controller
public class ClientLocationController {
	@Autowired
	ClientLocationDao clientLocationDao;
	
	@Autowired
	ClientDao clientDao;
	
	public Location addLocation(String location){
		Location newLocation = null;
		try{
			Location addLocation = new Location();
			addLocation.setLocation(location);
			addLocation.setIsActive((short)1);
			addLocation.setRowCreated(new Date());
			//Location addNewLocation = clientLocationDao.addLocation(addLocation);
			//newLocation = setLocation(addNewLocation);
			newLocation = clientLocationDao.addLocation(addLocation);
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return newLocation;
	}
	
	
	@RequestMapping(value="/addNewClient",method = RequestMethod.POST)
	@ResponseBody
	public Map<Integer, List<Client>> addNewClient(@RequestParam("clientName") String clientName, @RequestParam("domain") String domain, @RequestParam("spoc") String spoc, @RequestParam("url") String url, @RequestParam(value="existsLocation") String existsLocation){
		
		Client newClient = new Client();
		newClient.setClient_name(clientName);
		newClient.setClient_url(url);
		newClient.setDomain(domain);
		newClient.setSpoc(spoc);
		newClient.setMail_id(spoc);
		newClient.setIs_active((short)1);
		newClient.setRowCreated(new Date());
		Client InsertedClient = clientDao.addRetriveClient(newClient);
		
		
		Location findLocation = clientLocationDao.matchByLocation(existsLocation);
		Location addLocationToClient = null;
		System.out.println("RESULT:::: "+findLocation);
		if(findLocation != null){
			addLocationToClient = findLocation;
		}
		else{
			addLocationToClient = addLocation(existsLocation);
		}
		
		/*
		 * Create Client Location
		 */
		
		ClientLocation clientLocation = new ClientLocation();
		clientLocation.setClientFk(InsertedClient);
		clientLocation.setLocationFk(addLocationToClient);
		clientLocation.setIs_active((short) 1);
		clientLocation.setRowCreated(new Date());
		clientDao.addClientLocation(clientLocation);
	
		Integer client_id = InsertedClient.getClient_id();
			List<Client> allClients = clientDao.getAllClients();
			//Client returnClient = setClient(InsertedClient);
			List<Client> returnList = new ArrayList<Client>();
			
			for(int i=0;i<allClients.size();i++){
				returnList.add(setClient(allClients.get(i)));
				
			}
						
			Map<Integer, List<Client>> x = new HashMap<Integer, List<Client>>();
			x.put(client_id, returnList);
		return x;
			
			}
	public Client setClient(Client getClient){
		Client client = new Client();
		client.setClient_id(getClient.getClient_id());
		client.setClient_name(getClient.getClient_name());
		client.setClient_url(getClient.getClient_url());
		client.setDomain(getClient.getDomain());
		client.setIs_active(getClient.getIs_active());
		client.setMail_id(getClient.getMail_id());
		client.setRowAltered(getClient.getRowAltered());
		client.setRowCreated(getClient.getRowCreated());
		client.setSpoc(getClient.getSpoc());
	return client;
	}
	
	public Location setLocation(Location location){
		Location returnLocation = new Location();
		returnLocation.setLocationId(location.getLocationId());
		returnLocation.setLocation(location.getLocation());
		returnLocation.setIsActive(location.getIsActive());
		returnLocation.setRowCreated(location.getRowCreated());
		returnLocation.setRowAltered(location.getRowAltered());
		return returnLocation;
	}
	
}
