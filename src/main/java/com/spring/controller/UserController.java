package com.spring.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.dao.TeamDAO;
import com.spring.dao.TeamUserDAO;
import com.spring.dao.UserDAO;
import com.spring.model.Role;
import com.spring.model.Team;
import com.spring.model.TeamUser;
import com.spring.model.User;
import com.spring.model.UserRole;

@Controller
public class UserController {

	@Autowired
	UserDAO userDao;
	
	@Autowired
	TeamDAO teamDao;
	
	@Autowired
	TeamUserDAO tUserDao;

	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	@RequestMapping(value = "/signIn", method = RequestMethod.POST)
	@ResponseBody
	public String signIn(Model model,
			@ModelAttribute("emailAddress") String emailAddress,
			@ModelAttribute("password") String password, HttpSession session) {
			String returnPage="";
			Boolean userExist = false;
			String authority = null;
		try {
			UserRole userRole = userDao.findEmail(emailAddress);
			if (userRole != null) {
				userExist = passwordEncoder.matches(password, userRole.getUserFk().getPassword());
				if(userExist){
					authority = userRole.getRoleFk().getAuthority();
					session.setAttribute("authority", authority);
					String currenUser = userRole.getUserFk().getUserName();
					if(authority !=null && authority!=""){
						if(authority.equals("ROLE_ADMIN")) returnPage = "adminPage";
						else  returnPage="trackers";
					}
				}else{
					returnPage="false";
				}
				
			}else{
				returnPage="false";
			}

		} catch (Exception e) {
			System.out.println(e);
		}
		return returnPage;
	}

	@RequestMapping(value = "/viewUser", method = RequestMethod.GET)
	public String user(Model model, @ModelAttribute("user") User user) {
		try {
			List<User> userList = userDao.userList();
			model.addAttribute("userList", userList);
			List<TeamUser> tUser = tUserDao.getList();
			model.addAttribute("tUser", tUser);
			
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return "User/list";
	}

	@RequestMapping(value = "/addUser", method = RequestMethod.GET)
	public String signUp(Model model) {
		try {
			List<Team> teamList =teamDao.teamList();
			model.addAttribute("teamList", teamList);
			model.addAttribute("user", new User());
		} catch (Exception e) {
			System.out.println(e);
		}
		return "User/insert";
	}

	@RequestMapping(value = "/saveUser", method = RequestMethod.POST)
	public String saveUser(Model model, @ModelAttribute("user") User user, @RequestParam("teams") Integer  teamId, RedirectAttributes redirectAttr) {
		try {
			if (user != null) {
				User userBean = new User();
				userBean.setUserName(user.getEmailAddress());
				userBean.setEmailAddress(user.getEmailAddress());
				userBean.setPassword(passwordEncoder.encode(user.getPassword()));
				if (user.getIsActive() != 1) userBean.setIsActive((short) 0);
				else userBean.setIsActive(user.getIsActive());
					userBean.setRowCreated(new Date());
					userBean.setEnabled(true);
					userBean.setAccountExpired(false);
					userBean.setAccountLocked(false);
					userBean.setPasswordExpired(false);
					Integer userId = userDao.save(userBean);
					
					if(userId !=null && userId !=0){
						UserRole uRole = new UserRole();
						uRole.setIsActive((short)1);
						uRole.setRowCreated(new Date());
						Role role = userDao.getRoleByName("ROLE_USER");
						uRole.setRoleFk(role);
						userBean.setUserId(userId);
						uRole.setUserFk(userBean);
						Integer uRoleId = userDao.saveUserRole(uRole);
							if (uRoleId != null && uRoleId != 0) {
								Team team = teamDao.getTeamById(teamId);
								TeamUser tUser = new TeamUser();
								tUser.setUserFk(userBean);
								tUser.setTeamFk(team);
								tUser.setIsActive((short) 1);
								tUser.setRowCreated(new Date());
								Integer newTeamUserId = tUserDao.save(tUser);
								if (newTeamUserId != null && newTeamUserId != 0) redirectAttr.addFlashAttribute("message", "User has been added successfully");
							}
					
					}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:viewUser";
	}


	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	public String updateUser(Model model, @ModelAttribute("tuser") TeamUser tuser, RedirectAttributes redirectAttr) {
		try {
			if (tuser != null) {
				Integer userId = tuser.getUserFk().getUserId();
				User userBean = userDao.getUserById(userId);
				userBean.setUserName(tuser.getUserFk().getEmailAddress());
				userBean.setEmailAddress(tuser.getUserFk().getEmailAddress());
				userBean.setPassword(passwordEncoder.encode(tuser.getUserFk().getPassword()));
				if (tuser.getIsActive() != 1) userBean.setIsActive((short) 0);
				else userBean.setIsActive(tuser.getUserFk().getIsActive());
					userBean.setRowAltered(new Date());
					userDao.update(userBean);
					if(userId !=null && userId !=0){
						Team selectedTeam =teamDao.getTeamById(tuser.getTeamFk().getTeamId());
						TeamUser tUser = tUserDao.getUserTeam(userId);
						tUser.setTeamFk(selectedTeam);
						tUser.setRowAltered(new Date());
						tUserDao.update(tUser);
						redirectAttr.addFlashAttribute("message", "User has been updated successfully");
					}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:viewUser";
	}
	
	
	@RequestMapping(value = "/editUser", method = RequestMethod.GET)
	public String editUser(Model model, @ModelAttribute("userId") Integer userId) {
		try {
			
			List<Team> teamList =teamDao.teamList();
			model.addAttribute("teamList", teamList);
			TeamUser tUser = tUserDao.getUserTeam(userId);
			model.addAttribute("tUser", tUser);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "User/edit";
	}

	@RequestMapping(value = "/viewRole", method = RequestMethod.GET)
	public String viewrole(Model model) {
		try {
			List<Role> roleList = userDao.roleList();
			model.addAttribute("roleList", roleList);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "Role/list";
	}

	@RequestMapping(value = "/addRole", method = RequestMethod.GET)
	public String addRole(Model model) {
		try {
			model.addAttribute("role", new Role());
		} catch (Exception e) {
			System.out.println(e);
		}
		return "Role/insert";
	}
	
	@RequestMapping(value = "/editRole", method = RequestMethod.GET)
	public String editRole(Model model, @RequestParam("roleId") Integer roleId) {
		try {
			
			Role role = userDao.getRoleById(roleId);
			model.addAttribute("role", role);
			
		} catch (Exception e) {
			System.out.println(e);
		}
		return "Role/edit";
	}

	@RequestMapping(value = "/saveRole", method = RequestMethod.POST)
	public String saveRole(Model model, @ModelAttribute("role") Role role,
			RedirectAttributes redirectAttr) {
		try {
			if (role != null) {
				String message="";
				Role roleBean = null;
				 Integer roleId = role.getId();
				if(roleId!=null && roleId!=0){
					roleBean = userDao.getRoleById(roleId);
					roleBean.setRowAltered(new Date());
				}else{
					 roleBean = new Role();
					 roleBean.setRowCreated(new Date());
				}
				roleBean.setAuthority(role.getAuthority());
				if (role.getIsActive() != 1) roleBean.setIsActive((short) 0);
				else roleBean.setIsActive(role.getIsActive());
				if(roleId!=null && roleId!=0){
					userDao.updateRole(roleBean);
					message = "Role '" +role.getAuthority()+"' has been updated successfully";
				}else{
					userDao.saveRole(roleBean);
					message = "Role '" +role.getAuthority()+"' has been updated successfully";
				}
				redirectAttr.addFlashAttribute("message", message);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:viewRole";
	}

	@RequestMapping(value = "/cancelRole", method = RequestMethod.GET)
	public String cancelRole(Model model) {
		try {

		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect: viewRole";
	}

	@RequestMapping(value = "/cancelUser", method = RequestMethod.GET)
	public String cancelUser(Model model) {
		try {

		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:viewUser";
	}
	@RequestMapping(value="/deleteUSer",method = RequestMethod.GET)
	public String deleteUser(Model model, @RequestParam("userId") Integer userId, RedirectAttributes redirectAttr){
		try{
			System.out.println("@@@@" +userId );
			User userBean = userDao.getUserById(userId);
			userBean.setIsActive((short)0);
			userDao.update(userBean);
			redirectAttr.addFlashAttribute("message","User has been made inactive");
		}catch(Exception e){System.out.println(e);}
		return "redirect:viewUser";
	}	

	@RequestMapping(value="/deleteRole",method = RequestMethod.GET)
	public String deleteRole(Model model, @RequestParam("roleId") Integer roleId, RedirectAttributes redirectAttr){
		try{
			Role role = userDao.getRoleById(roleId);
			role.setIsActive((short)0);
			userDao.updateRole(role);
			redirectAttr.addFlashAttribute("message","Role has been made inactive");
		}catch(Exception e){System.out.println(e);}
		return "redirect: viewRole";
	}	

}
