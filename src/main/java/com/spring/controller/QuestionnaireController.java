package com.spring.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.dao.QuestionnaireDao;
import com.spring.dao.TrackerDao;
import com.spring.model.Questionnaire;
import com.spring.model.Tracker;
import com.spring.model.TrackerQuestionnaire;

@Controller
public class QuestionnaireController {

	@Autowired
	QuestionnaireDao questionnaireDao;
	
	@Autowired
	TrackerDao trackerDao;
	
	@RequestMapping(value="/quesList",method = RequestMethod.GET)
	public String quesList(Model model){
		try {
		List<TrackerQuestionnaire> allQues = questionnaireDao.getAllQuestions();
		model.addAttribute("allQues",allQues);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "ManageQuestionnaire/list";
	}
	
	@RequestMapping(value="/getQuestionnaireByTracker",method = RequestMethod.GET)
	public String getQuestionnaireByTracker(Model model,@RequestParam("tracker_id") int trackerId){
		try {
		List<TrackerQuestionnaire> allQues = questionnaireDao.getByTracker(trackerId);
		System.out.println("ALQUES::: "+allQues);
		model.addAttribute("allQues",allQues);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "ManageQuestionnaire/list";
	}
	
	@RequestMapping(value="/addQues")
	public String addQues(Model model){
		try {
		List<Tracker> trackerList = trackerDao.getAllTrackers();
		model.addAttribute("allTrackers", trackerList);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "ManageQuestionnaire/create";
	}
	
	@RequestMapping(value="/saveQues",method = RequestMethod.POST)
	public String saveQues(@RequestParam("field") String questionnaire_name,@RequestParam("trackerFk") Integer tracker_id,
			@RequestParam("is_active") Short is_active,RedirectAttributes redirectAttr){
		try {
		Tracker tracker = trackerDao.getTracker(tracker_id);
		TrackerQuestionnaire ques = new TrackerQuestionnaire();
		ques.setField(questionnaire_name);
		ques.setIs_active(is_active);
		ques.setRowCreated(new Date());
		ques.setTrackerFk(tracker);
		
		questionnaireDao.addQues(ques);
		redirectAttr.addFlashAttribute("message", "Questionnaire has been added successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:quesList";
	}
	
	@RequestMapping(value="/editQues")
	public String editQues(Model model,@RequestParam("questionnaire_id") Integer questionnaire_id){
		try {
		TrackerQuestionnaire questionnaire = questionnaireDao.getQuesById(questionnaire_id);
		model.addAttribute("questionnaire", questionnaire);
		
		List<Tracker> trackerList = trackerDao.getAllTrackers();
		model.addAttribute("trackerList", trackerList);
		} catch (Exception e) {
			System.out.println(e);
		}
		return "ManageQuestionnaire/edit";
	}
	
	@RequestMapping(value="/updateQues")
	public String updateQues(Model model,@RequestParam("trackerQuestionnaireId") Integer questionnaire_id,
			@RequestParam("trackerFk") Integer tracker_id,
			@RequestParam("field") String questionnaire_name,
			@RequestParam("is_active") Short is_active,RedirectAttributes redirectAttr){
		try {
		Tracker tracker = trackerDao.getTracker(tracker_id);
		
		TrackerQuestionnaire ques = questionnaireDao.getQuesById(questionnaire_id);
		ques.setTrackerQuestionnaireId(questionnaire_id);
		ques.setField(questionnaire_name);
		ques.setIs_active(is_active);
		ques.setRowAltered(new Date());
		//ques.setRowCreated(new Date());
		ques.setTrackerFk(tracker);
		questionnaireDao.updateQues(ques);
		redirectAttr.addFlashAttribute("message", "Questionnaire has been updated successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:quesList";
	}
	
	@RequestMapping(value="/deleteQues")
	public String deleteQues(@RequestParam("questionnaire_id") Integer questionnaire_id,RedirectAttributes redirectAttr){
		try {
		questionnaireDao.removeQues(questionnaire_id);
		redirectAttr.addFlashAttribute("message", "Questionnaire has been deleted successfully");
		} catch (Exception e) {
			System.out.println(e);
		}
		return "redirect:quesList";
	}
	
}
