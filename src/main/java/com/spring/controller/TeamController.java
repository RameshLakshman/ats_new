package com.spring.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spring.dao.TeamDAO;
import com.spring.dao.UserDAO;
import com.spring.model.Role;
import com.spring.model.Team;
import com.spring.model.User;
import com.spring.model.UserRole;


@Controller
public class TeamController {

	@Autowired
	TeamDAO teamDao;
	
	@Autowired
	UserDAO userDao;
	
	@Autowired
	BCryptPasswordEncoder passwordEncoder;
	
	@RequestMapping(value="/viewTeam",method = RequestMethod.GET)
	public String viewTeam(Model model){
		try{
			List<Team> teamList = teamDao.teamList();
			model.addAttribute("teamList", teamList);
		}catch(Exception e){System.out.println(e);}
		return "Team/list";
	}	
	
	@RequestMapping(value="/addTeam",method = RequestMethod.GET)
	public String signUp(Model model){
		model.addAttribute("team", new Team());
		return "Team/insert";
	}	
	
	@RequestMapping(value="/saveTeam",method = RequestMethod.POST)
	public String saveTeam(Model model, @ModelAttribute("team") Team team, RedirectAttributes redirectAttr){
		String message = "";
		try{
			Team teamBean = null;
			Integer teamId = team.getTeamId();
			
			if(teamId !=null && teamId !=0){
				teamBean = teamDao.getTeamById(teamId);
				
			}else{
				teamBean = new Team();
			}
			
			teamBean.setTeamName(team.getTeamName());
			if(team.getIsActive() !=1) teamBean.setIsActive((short)0);
			else teamBean.setIsActive(team.getIsActive());
			
			if(teamId !=null && teamId !=0){
				teamBean.setRowAltered(new Date());
				teamDao.update(teamBean);
				Integer userId = team.getUserFk().getUserId();
				User userBean = userDao.getUserById(userId);
				userBean.setUserName(team.getUserFk().getEmailAddress());
				userBean.setEmailAddress(team.getUserFk().getEmailAddress());
				userBean.setPassword(passwordEncoder.encode(team.getUserFk().getPassword()));
				if (team.getIsActive() != 1) userBean.setIsActive((short) 0);
				else userBean.setIsActive(team.getUserFk().getIsActive());
				userBean.setRowAltered(new Date());
				userDao.update(userBean);
				message = "Team '" +team.getTeamName()+"' has been updated successfully";
			}else{
				String loginEmail = team.getUserFk().getEmailAddress();
				String loginPassword = team.getUserFk().getPassword();
				if (loginEmail != null && loginEmail != ""
						&& loginPassword != null && loginPassword != "") {
					User userBean = new User();
					userBean.setUserName(loginEmail);
					userBean.setEmailAddress(loginEmail);
					userBean.setPassword(passwordEncoder.encode(loginPassword));
					if (team.getIsActive() != 1)
						userBean.setIsActive((short) 0);
					else
						userBean.setIsActive(team.getIsActive());
					userBean.setRowCreated(new Date());
					userBean.setEnabled(true);
					userBean.setAccountExpired(false);
					userBean.setAccountLocked(false);
					userBean.setPasswordExpired(false);
					Integer userId = userDao.save(userBean);
					if (userId != null && userId != 0) {
						if (userId != null && userId != 0) {
							UserRole uRole = new UserRole();
							uRole.setIsActive((short) 1);
							uRole.setRowCreated(new Date());
							Role role = userDao.getRoleByName("TEAM_LEAD");
							uRole.setRoleFk(role);
							userBean.setUserId(userId);
							uRole.setUserFk(userBean);
							Integer uRoleId = userDao.saveUserRole(uRole);
							if (uRoleId != null && uRoleId != 0) {
								teamBean.setUserFk(userBean);
							}
						}

					}
				}
				if(teamBean.getUserFk()!=null){
					teamBean.setRowCreated(new Date());
					teamDao.save(teamBean);
					message = "Team '" +team.getTeamName()+"' has been added successfully";
				}
			}
			redirectAttr.addFlashAttribute("message",message);
		}catch(Exception e){System.out.println(e);}
		return "redirect: viewTeam";
	}	
	
	@RequestMapping(value="/editTeam",method = RequestMethod.GET)
	public String editTeam(Model model, @RequestParam("teamId") Integer teamId){
		try{
			Team team = teamDao.getTeamById(teamId);
			if(team!=null)
				model.addAttribute("team", team);
		}catch(Exception e){System.out.println(e);}
		
		
		return "Team/edit";
	}	
	
	@RequestMapping(value="/cancelTeam",method = RequestMethod.GET)
	public String cancelTeam(Model model){
		try{
			
		}catch(Exception e){System.out.println(e);}
		return "redirect: viewTeam";
	}	
	
	
	@RequestMapping(value="/deleteTeam",method = RequestMethod.GET)
	public String deleteTeam(Model model, @RequestParam("teamId") Integer teamId, RedirectAttributes redirectAttr){
		try{
			Team team = teamDao.getTeamById(teamId);
			team.setIsActive((short)0);
			teamDao.update(team);
			redirectAttr.addFlashAttribute("message","Team has been made inactive");
		}catch(Exception e){System.out.println(e);}
		return "redirect: viewTeam";
	}	
	
}
