/**
 * 
 */
package com.spring.daoImpl;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.spring.dao.CandidateDAO;
import com.spring.model.Candidates;

/**
 * @author Ramesh
 *
 */
@Repository
@Transactional
public class CandidateDaoImplementation implements CandidateDAO {

	@PersistenceContext
	private EntityManager entityManager;

	/*
	 * (non-Javadoc)
	 * @see com.spring.dao.CandidateDAO#candidateList() 
	 * get Candidate List
	 */
	public List<Candidates> candidateList() {
		List<Candidates> candidate = null;
		try {
			candidate = entityManager.createQuery("FROM Candidates",
					Candidates.class).getResultList();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return candidate;
	}

}
