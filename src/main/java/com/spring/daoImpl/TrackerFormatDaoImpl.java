package com.spring.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dao.TrackerFormatDao;
import com.spring.model.Tracker;
import com.spring.model.TrackerFormat;
import com.spring.model.TrackerQuestionnaire;
import com.spring.model.UserRole;

@Repository
@Transactional
public class TrackerFormatDaoImpl implements TrackerFormatDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public List<TrackerFormat> viewTrackerFormat() {
		List<TrackerFormat> trackerFormatList = new ArrayList<TrackerFormat>();
		try{
			trackerFormatList = manager.createQuery("From TrackerFormat").getResultList();
			System.out.println(":::::::::::::: "+trackerFormatList);
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}		
		
		return trackerFormatList;
	}

	public List<TrackerFormat> getByTracker(int trackerId) {
		List<TrackerFormat> trackerFormatList = new ArrayList<TrackerFormat>();
		try{
			TypedQuery<TrackerFormat> query =  manager.createQuery("From TrackerFormat t where t.trackerFk.trackerId=:trackerId",TrackerFormat.class);
			trackerFormatList = query.setParameter("trackerId", trackerId).getResultList();
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}		
		
		return trackerFormatList;
	}

	public TrackerFormat getTrackerFormat(int trackerFormatId) {
		
		return manager.find(TrackerFormat.class, trackerFormatId);
	}

	public TrackerFormat update(TrackerFormat trackerFormat) {
		TrackerFormat getTrackerFormat = null;
		try{
		manager.merge(trackerFormat);
		manager.flush();
		getTrackerFormat = trackerFormat;
		}
		catch(Exception e){
			getTrackerFormat = null;
		}
		return getTrackerFormat;
	}

}
