package com.spring.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dao.ClientLocationDao;
import com.spring.model.Client;
import com.spring.model.Location;
import com.spring.model.Role;
@Repository
@Transactional
public class ClientLocationDaoImpl implements ClientLocationDao {

	@PersistenceContext
	private EntityManager manager;
	
	public Location addLocation(Location location) {
		Location inserLocation = null;
		try{
		    manager.persist(location);
		    manager.flush();
		    inserLocation = location;
		}
		catch(Exception e){
			System.out.println(e);
		}
		
		
		return inserLocation;
	}

	public List<Location> getAllLocations() {
		List<Location> locations = null;
		try {
			locations = manager.createQuery("From Location",Location.class).getResultList();
		}
        catch(Exception e){
			System.out.println(e);
		}
		return locations;
	}

	public Location matchByLocation(String locationName) {
		Location inserLocation = null;
		try{
			TypedQuery<Location> query = manager.createQuery("from Location u where u.location=:location",Location.class);
			inserLocation = query.setParameter("location",locationName).getSingleResult();
			}catch(Exception e){
				inserLocation = null;
				System.out.println(e);
			}
		
		return inserLocation;
	}

}
