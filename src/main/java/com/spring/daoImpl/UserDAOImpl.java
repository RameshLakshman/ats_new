package com.spring.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dao.UserDAO;
import com.spring.model.Role;
import com.spring.model.User;
import com.spring.model.UserRole;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

	@PersistenceContext
	private EntityManager manager;
	
	
	public Integer save(User userBean) {
		// TODO Auto-generated method stub
		try{
			manager.persist(userBean);
		}catch(Exception e){
			System.out.println(e);
		}
		return userBean.getUserId();
	}


	public UserRole findEmail(String username) throws Exception {
		// TODO Auto-generated method stub
		UserRole user =null;
		try{
		TypedQuery<UserRole> query = manager.createQuery("from UserRole u where u.userFk.emailAddress=:name",UserRole.class);
		user = query.setParameter("name",username).getSingleResult() ;
		}catch(Exception e){
			user = null;
			System.out.println(e);
		}
		return user;
	}


	public List<User> userList() {
		// TODO Auto-generated method stub
		List<User> userList = null;
		try{
			userList = manager.createQuery("from User", User.class).getResultList();
		}catch(Exception e){System.out.println(e);}
		
		return userList;
	}


	public List<Role> roleList() {
		// TODO Auto-generated method stub
		List<Role> roleList = null;
		try{
			roleList = manager.createQuery("from Role", Role.class).getResultList();
		}catch(Exception e){System.out.println(e);}
		return roleList;
	}


	public void saveRole(Role roleBean) {
		// TODO Auto-generated method stub
		manager.persist(roleBean);
	}


	public Role getRoleById(Integer roleId) {
		// TODO Auto-generated method stub
		Role role = manager.find(Role.class, roleId);
		return role;
	}


	public void updateRole(Role roleBean) {
		// TODO Auto-generated method stub
		manager.merge(roleBean);
	}


	public void update(User userBean) {
		// TODO Auto-generated method stub
		manager.merge(userBean);
		
	}


	public User getUserById(Integer userId) {
		// TODO Auto-generated method stub
		User user = manager.find(User.class,userId);
		return user;
	}


	public Role getRoleByName(String roleName) {
		Role role=null;
		try{
			TypedQuery<Role> query = manager.createQuery("from Role u where u.authority=:name",Role.class);
			role = query.setParameter("name",roleName).getSingleResult();
			}catch(Exception e){
				role = null;
				System.out.println(e);
			}
		return role;
	}


	public Integer saveUserRole(UserRole uRole) {
		// TODO Auto-generated method stub
		manager.persist(uRole);
		return uRole.getUserRoleid();
	}

}
