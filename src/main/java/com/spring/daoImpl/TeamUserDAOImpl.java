package com.spring.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dao.TeamUserDAO;
import com.spring.model.TeamUser;
import com.spring.model.User;

@Repository
@Transactional
public class TeamUserDAOImpl implements TeamUserDAO {

	@PersistenceContext
	private EntityManager manager;
	
	public List<TeamUser> getList() {
		// TODO Auto-generated method stub
		List<TeamUser> getList = null;
		try{
			getList = manager.createQuery("from TeamUser", TeamUser.class).getResultList();
		}catch(Exception e){System.out.println(e);}
		return getList;
	}

	public Integer save(TeamUser tUser) {
		// TODO Auto-generated method stub
		manager.persist(tUser);
		return tUser.getTeamUserId();
	}

	public TeamUser getUserTeam(Integer userId) {
		// TODO Auto-generated method stub
		TeamUser tUser = null;
		try{
			TypedQuery<TeamUser> query = manager.createQuery("from TeamUser u where u.userFk.userId=:id",TeamUser.class);
			tUser = query.setParameter("id",userId).getSingleResult();
		}catch(Exception e){System.out.println(e);}
		return tUser;
	}

	public void update(TeamUser tuser) {
		// TODO Auto-generated method stub
		manager.merge(tuser);
	}

}
