package com.spring.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dao.ClientDao;
import com.spring.model.Client;
import com.spring.model.ClientLocation;

@Repository
@Transactional
public class ClientDaoImpl implements ClientDao {

	@PersistenceContext
	private EntityManager manager;
	
	
	
	public List<Client> getAllClients() {
		// TODO Auto-generated method stub
		List<Client> clients = null;
		try {
		clients = manager.createQuery("From Client",Client.class).getResultList();
		}
        catch(Exception e){
			System.out.println(e);
		}
		return clients;
	}

	public void addClient(Client Client) {
		// TODO Auto-generated method stub
		try{
		    manager.persist(Client);
		}
		catch(Exception e){
			System.out.println(e);
		}
	}

	public Client getClientById(int client_id) {
		// TODO Auto-generated method stub
		return manager.find(Client.class, client_id);
	}

	public void updateClient(Client client) {
		// TODO Auto-generated method stub
		try{
		manager.merge(client);
		}
		catch(Exception e){
			System.out.println(e);
		}
	}

	public void removeClient(int client_id) {
		// TODO Auto-generated method stub
		try {
		Client client = (Client)manager.find(Client.class, client_id);
		manager.remove(client);
		}
		catch (Exception e){
            e.printStackTrace();
        } 
	}
	/*
	 * Add and retrive Client
	 */
	public Client addRetriveClient(Client client) {
		Client insertClient = null;
		try{
		    manager.persist(client);
		    manager.flush();
		    insertClient = client;
		}
		catch(Exception e){
			System.out.println(e);
		}
		return insertClient;
	}

	public void addClientLocation(ClientLocation clientLocation) {
	
		try{
		    manager.persist(clientLocation);
		}
		catch(Exception e){
			System.out.println(e);
		}
		
	}
	
	public List<Client> getClientByName(String name) {
		
		// TODO Auto-generated method stub
	
		return manager.createQuery(
			    "SELECT c FROM Client c WHERE UPPER(c.client_name) LIKE :custName",Client.class)
			    .setParameter("custName","%" + name.toUpperCase() + "%")
			    .getResultList();
		
	}
	
}
