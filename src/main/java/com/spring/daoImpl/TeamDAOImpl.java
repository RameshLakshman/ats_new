package com.spring.daoImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dao.TeamDAO;
import com.spring.model.Team;
import com.spring.model.User;


@Repository
@Transactional
public class TeamDAOImpl implements TeamDAO {

	
	@PersistenceContext
	private EntityManager manager;

	public List<Team> teamList() {
		List<Team> teamList = null;
		try{
			teamList = manager.createQuery("from Team", Team.class).getResultList();
		}catch(Exception e){System.out.println(e);}
		return teamList;
	}

	public void save(Team team) {
		// TODO Auto-generated method stub
		if(team!=null)
			manager.persist(team);
	}

	public Team getTeamById(Integer teamId) {
		// TODO Auto-generated method stub
		Team returnValue=null;
		try{
			Team team = manager.find(Team.class, teamId);
			if(team !=null){
				returnValue = team;
			}
		}catch(Exception e){System.out.println(e);}
		
		return returnValue;
	}
	public void update(Team teamBean) {
		manager.merge(teamBean);
		
	}
}
