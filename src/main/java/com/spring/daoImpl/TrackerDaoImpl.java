package com.spring.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.RollbackException;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring.dao.TrackerDao;
import com.spring.model.Location;
import com.spring.model.Questionnaire;
import com.spring.model.Tracker;
import com.spring.model.TrackerFormat;
import com.spring.model.TrackerFormatName;
import com.spring.model.TrackerQuestionnaire;

@Repository
@Transactional
public class TrackerDaoImpl implements TrackerDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public Integer save(Tracker tracker) {
		Integer trackerId = null;
		try{
			manager.persist(tracker);
			manager.flush();
			trackerId = tracker.getTrackerId(); 

			
		}catch(RollbackException e){
			
			System.out.println(e);
			
		}
		
		return trackerId;
	}

	@SuppressWarnings("unchecked")
	public List<Tracker> getAllTrackers() {
		List<Tracker> trackerList = null;
		try{
			trackerList = manager.createQuery("From Tracker").getResultList();
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}		
		
		return trackerList;
	}

	public Tracker getTracker(int tracker_id) {
		Tracker tracker = null;
		try{
			tracker = manager.find(Tracker.class, tracker_id);
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		return tracker;
	}

	public void updateTracker(Tracker tracker) {
		try{
			manager.merge(tracker);
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		
	}

	public void deleteTracker(int tracker_id) {
		try{
			Tracker tracker = (Tracker)manager.find(Tracker.class, tracker_id);
			manager.remove(tracker);
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}	
	}

	@SuppressWarnings("unchecked")
	public List<Location> getLocations() {
		List<Location> locationList = new ArrayList<Location>();
		try{
			locationList = manager.createQuery("From Location").getResultList();
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}		
		
		return locationList;
	}

	public Location getLocation(int location_id) {
		Location location = null;
		try{
			location = manager.find(Location.class, location_id);
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		return location;
	}
	
	
	public List<Tracker> searchTrackers(int client_id,int expFrom, int expTo,
			String skill) {
		// TODO Auto-generated method stub
		List<Tracker> trackers = null;
		trackers =manager.createQuery("SELECT t FROM Tracker t WHERE t.clientFk.client_id=:client AND UPPER(t.primarySkills) LIKE :skills AND t.experienceFrom=:expFrm AND t.experienceTo=:expToo",Tracker.class)
				.setParameter("client", client_id)
				.setParameter("skills", "%" + skill.toUpperCase() + "%")
                .setParameter("expFrm", expFrom)
                .setParameter("expToo", expTo)
                .getResultList();
			
		return trackers;
	}

	
	
	@SuppressWarnings("unchecked")
	public List<TrackerFormat> TrackerFormatList() {
		List<TrackerFormat> trackerFormat = new ArrayList<TrackerFormat>();
		try{
			trackerFormat = manager.createQuery("from TrackerFormat").getResultList();
			for(int i=0;i<trackerFormat.size();i++){
			}
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}	
		return trackerFormat;
	}

	public List<TrackerFormat> TrackerFormatByTracker(int tId) {
		List<TrackerFormat> tfList = null;
		try{
			TypedQuery<TrackerFormat> query = manager.createQuery("from TrackerFormat u where u.trackerFk.trackerId=:trackerId",TrackerFormat.class);
			tfList = query.setParameter("trackerId",tId).getResultList();
		}catch(Exception e){
			System.out.println(e);
		}
		return tfList;
	}

	public void saveTrackerFormat(TrackerFormat tFormat) {
		// TODO Auto-generated method stub
		manager.merge(tFormat);
	}

	@SuppressWarnings("unchecked")
	public List<TrackerFormatName> getTFNameList() {
		List<TrackerFormatName> trackerFormatName =null;
		try{
			trackerFormatName = manager.createQuery("From TrackerFormatName").getResultList();
		}catch(Exception e){System.out.println(e);}
		
		return trackerFormatName;
	}

	public TrackerFormatName findTrackerFormatName(String tfname) {
		// TODO Auto-generated method stub
		TrackerFormatName trackerFormatName =null;
		try{
		TypedQuery<TrackerFormatName> query = manager.createQuery("from TrackerFormatName u where u.trackerFormatName=:name",TrackerFormatName.class);
		trackerFormatName = query.setParameter("name",tfname).getSingleResult() ;
		}catch(Exception e){
			trackerFormatName = null;
			System.out.println(e);
		}
		return trackerFormatName;
	}

	public TrackerFormatName saveTrackerFormatName(TrackerFormatName insert) {
		// TODO Auto-generated method stub
		manager.persist(insert);
		return insert;
	}

	public TrackerFormatName trackerFormatNameById(Integer tfnId) {
		TrackerFormatName trackerfn = null;
		try{
			TypedQuery<TrackerFormatName> query = manager.createQuery("from TrackerFormatName u where u.id=:tfnameId",TrackerFormatName.class);
			trackerfn = query.setParameter("tfnameId",tfnId).getSingleResult();
		}catch(Exception e){
			System.out.println(e);
		}
		return trackerfn;
	}

	public Tracker trackerBytfnFk(Integer tfnId) {
		List<Tracker> trackerLis = null;
		Tracker tracker = null;
		try{
			TypedQuery<Tracker> query = manager.createQuery("from Tracker u where u.tfNamefk.id=:tfnameId",Tracker.class);
			trackerLis = query.setParameter("tfnameId",tfnId).setMaxResults(1).getResultList();
			if(trackerLis!=null && trackerLis.size()>0){
				tracker = trackerLis.get(0);
			}
		}catch(Exception e){ 
			System.out.println(e);
		}
		return tracker;
	}

	public Tracker trackerByquestionnaireFk(int quesId) {
		List<Tracker> trackerLis = null;
		Tracker tracker = null;
		try{
			TypedQuery<Tracker> query = manager.createQuery("from Tracker u where u.questionnaireFk.questionnaireId=:quesId",Tracker.class);
			trackerLis = query.setParameter("quesId",quesId).setMaxResults(1).getResultList();
			if(trackerLis!=null && trackerLis.size()>0){
				tracker = trackerLis.get(0);
			}
		}catch(Exception e){ 
			System.out.println(e);
		}
		return tracker;
	}

	public Questionnaire findQuestionnaireName(String questionname) {
		Questionnaire Questionnaire =null;
		try{
		TypedQuery<Questionnaire> query = manager.createQuery("from Questionnaire u where u.questionnaireName=:name",Questionnaire.class);
		Questionnaire = query.setParameter("name",questionname).getSingleResult() ;
		}catch(Exception e){
			Questionnaire = null;
			System.out.println(e);
		}
		return Questionnaire;
	}

	public Questionnaire saveQuestionnaireName(Questionnaire insert) {
		manager.persist(insert);
		return insert;
	}

	public List<Questionnaire> getTQuestionList() {
		List<Questionnaire> Questionnaire =null;
		try{
			Questionnaire = manager.createQuery("From Questionnaire").getResultList();
		}catch(Exception e){System.out.println(e);}
		
		return Questionnaire;
	}

	public Questionnaire questionnaireById(Integer tqnId) {
		Questionnaire trackerquestion = null;
		try{
			TypedQuery<Questionnaire> query = manager.createQuery("from Questionnaire u where u.questionnaireId=:tqnameId",Questionnaire.class);
			trackerquestion = query.setParameter("tqnameId",tqnId).getSingleResult();
		}catch(Exception e){
			System.out.println(e);
		}
		return trackerquestion;
	}

	public void saveTrackerQuestionnaire(TrackerQuestionnaire tqus) {
		manager.persist(tqus);
		
	}

	public List<TrackerQuestionnaire> trackerQuestionnaireByTracker( Integer trackerId) {
		List<TrackerQuestionnaire> tqList = null;
		try{
			TypedQuery<TrackerQuestionnaire> query = manager.createQuery("from TrackerQuestionnaire u where u.trackerFk.trackerId=:trackerId",TrackerQuestionnaire.class);
			tqList = query.setParameter("trackerId",trackerId).getResultList();
		}catch(Exception e){
			System.out.println(e);
		}
		return tqList;
	}
	
	

}
