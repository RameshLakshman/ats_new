package com.spring.daoImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.TypedQuery;

import com.spring.dao.QuestionnaireDao;
import com.spring.model.Questionnaire;
import com.spring.model.TrackerQuestionnaire;
import com.spring.model.TrackerFormat;

@Repository
@Transactional
public class QuestionnaireDaoImpl implements QuestionnaireDao{

	@PersistenceContext
	private EntityManager manager;
	
	public List<TrackerQuestionnaire> getAllQuestions() {
		// TODO Auto-generated method stub
		List<TrackerQuestionnaire> ques = null;
		try{
		 ques = manager.createQuery("From TrackerQuestionnaire",TrackerQuestionnaire.class).getResultList();
		}
		catch(Exception e){
			System.out.println(e);
		}
        return ques;
	}

	public void addQues(TrackerQuestionnaire ques) {
		// TODO Auto-generated method stub
		try{
			manager.persist(ques);
			}
			catch(Exception e){
				System.out.println(e);
			}
	}

	public TrackerQuestionnaire getQuesById(int questionnaire_id) {
		// TODO Auto-generated method stub
		return manager.find(TrackerQuestionnaire.class, questionnaire_id);
	}

	public void updateQues(TrackerQuestionnaire ques) {
		// TODO Auto-generated method stub
		try{
		manager.merge(ques);
		}
		catch(Exception e){
			System.out.println(e);
		}
	}

	public void removeQues(int questionnaire_id) {
		// TODO Auto-generated method stub
		try {
			TrackerQuestionnaire ques = (TrackerQuestionnaire)manager.find(TrackerQuestionnaire.class, questionnaire_id);
			manager.remove(ques);
			}
			catch (Exception e){
	            e.printStackTrace();
	        } 
	}
	
	public List<TrackerQuestionnaire> getByTracker(int trackerId) {
		List<TrackerQuestionnaire> questionnaireList = new ArrayList<TrackerQuestionnaire>();
		try{
			TypedQuery<TrackerQuestionnaire> query =  manager.createQuery("From TrackerQuestionnaire q where q.trackerFk.trackerId=:trackerId",TrackerQuestionnaire.class);
			questionnaireList = query.setParameter("trackerId", trackerId).getResultList();
		}		
		catch(Exception e)
		{
			e.printStackTrace();
		}		
		
		return questionnaireList;
	}


}
