package com.spring.dao;

import java.util.List;

import com.spring.model.TeamUser;

public interface TeamUserDAO {

	List<TeamUser> getList();

	Integer save(TeamUser tUser);

	TeamUser getUserTeam(Integer userId);

	void update(TeamUser tuser);

}
