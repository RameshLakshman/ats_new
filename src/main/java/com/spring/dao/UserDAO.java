package com.spring.dao;

import java.util.List;

import com.spring.model.Role;
import com.spring.model.User;
import com.spring.model.UserRole;

public interface UserDAO {

	Integer save(User userBean);

	UserRole findEmail(String username) throws Exception;

	List<User> userList();

	List<Role> roleList();

	void saveRole(Role roleBean);

	Role getRoleById(Integer roleId);

	void updateRole(Role roleBean);

	void update(User userBean);

	User getUserById(Integer userId);

	Role getRoleByName(String roleName);

	Integer saveUserRole(UserRole uRole);

}
