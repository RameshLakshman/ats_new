package com.spring.dao;

import java.util.List;

import com.spring.model.Location;

public interface ClientLocationDao {

	public Location addLocation(Location location);
	public List<Location> getAllLocations();
	public Location matchByLocation(String locationName);

}
