package com.spring.dao;

import java.util.List;

import com.spring.model.Team;

public interface TeamDAO {

	List<Team> teamList();

	void save(Team team);

	Team getTeamById(Integer teamId);

	void update(Team teamBean);

}
