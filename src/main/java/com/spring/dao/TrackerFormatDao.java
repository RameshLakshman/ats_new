package com.spring.dao;

import java.util.List;

import com.spring.model.TrackerFormat;

public interface TrackerFormatDao {

	public List<TrackerFormat> viewTrackerFormat();
	public List<TrackerFormat> getByTracker(int trackerId);
	public TrackerFormat getTrackerFormat(int trackerFormatId);
	public TrackerFormat update(TrackerFormat trackerFormat);
	
	
}
