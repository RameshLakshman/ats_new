package com.spring.dao;

import java.util.List;

import com.spring.model.Location;
import com.spring.model.Questionnaire;
import com.spring.model.Tracker;
import com.spring.model.TrackerFormat;
import com.spring.model.TrackerFormatName;
import com.spring.model.TrackerQuestionnaire;

public interface TrackerDao {
	
	public Integer save(Tracker tracker);
	public List<Tracker> getAllTrackers();
	public Tracker getTracker(int tracker_id);
	public void updateTracker(Tracker tracker);
	public void deleteTracker(int tracker_id);
	
	public List<Tracker> searchTrackers(int client_id,int expFrom,int expTo,String skill);
	public Tracker trackerBytfnFk(Integer tfnId);
	public Tracker trackerByquestionnaireFk(int quesId);
	
	
	public List<Location> getLocations();
	public Location getLocation(int location_id);
	
	List<TrackerFormat> TrackerFormatList();
	List<TrackerFormat> TrackerFormatByTracker(int tId);
	void saveTrackerFormat(TrackerFormat tFormat);
	
	
	List<TrackerFormatName> getTFNameList();
	TrackerFormatName findTrackerFormatName(String tfname);
	TrackerFormatName saveTrackerFormatName(TrackerFormatName insert);
	TrackerFormatName trackerFormatNameById(Integer tfnId);
	
	
	public Questionnaire findQuestionnaireName(String questionname);
	public Questionnaire saveQuestionnaireName(Questionnaire insert);
	public List<Questionnaire> getTQuestionList();
	public Questionnaire questionnaireById(Integer tqnId);
	
	public void saveTrackerQuestionnaire(TrackerQuestionnaire tqus);
	public List<TrackerQuestionnaire> trackerQuestionnaireByTracker(Integer trackerId);
	
	
	
}
