package com.spring.dao;

import java.util.List;

import com.spring.model.Client;
import com.spring.model.ClientLocation;

public interface ClientDao {

	public List<Client> getAllClients();
	public void addClient(Client Client);
	public Client getClientById(int client_id);
    public void updateClient(Client client);
    public void removeClient(int client_id);
    public Client addRetriveClient(Client client);
    public void addClientLocation(ClientLocation clientLocation);
    public List<Client> getClientByName(String name);
}
