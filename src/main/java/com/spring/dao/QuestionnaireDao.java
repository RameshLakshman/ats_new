package com.spring.dao;

import java.util.List;

import com.spring.model.Client;
import com.spring.model.Questionnaire;
import com.spring.model.TrackerQuestionnaire;

public interface QuestionnaireDao {

	public List<TrackerQuestionnaire> getAllQuestions();

	public List<TrackerQuestionnaire> getByTracker(int trackerId);
	public void addQues(TrackerQuestionnaire ques);
	public TrackerQuestionnaire getQuesById(int questionnaire_id);
    public void updateQues(TrackerQuestionnaire ques);
    public void removeQues(int questionnaire_id);
}
