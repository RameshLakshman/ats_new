/**
 * 
 */
package com.spring.dao;

import java.util.List;

import com.spring.model.Candidates;

/**
 * @author Ramesh
 *
 */
public interface CandidateDAO {

	
	public List<Candidates> candidateList();
	
}
