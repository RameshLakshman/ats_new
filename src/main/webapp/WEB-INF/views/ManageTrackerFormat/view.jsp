
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Manage Tracker Format</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Manage Tracker Format</strong></li>
					</ol>
				</div>            
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span style="color: red">${message}</span>
								<div class="ibox-tools">
								<!-- <a href="addTracker" class="btn btn-primary btn-xs"><i
										class="fa fa-pencil"></i> Add New</a> -->
									<!-- <a href="addStatus" class="btn btn-primary btn-xs"><i
										class="fa fa-pencil"></i> Add New</a> -->
								</div>
							</div>
							<div class="ibox-content">
								<table
									class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											 <th>Tracker Id</th> 
											<th>Field</th>
											<th>Row Created</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<c:choose>
										<c:when test="${allTrackerFormats != null}">
											<c:forEach items="${allTrackerFormats}" var="trackerFormat">
												<tr>
												<td><c:out value="${trackerFormat.trackerFormatId}" /></td>
													 <td><c:out value="${trackerFormat.trackerFk.trackerId}" /></td> 
													<td><c:out value="${trackerFormat.field}" /></td>
													<td><c:out value="${trackerFormat.rowCreated}" /></td>
													<td>
													<a href="editrackerFormat?tracker_format_id=<c:out value="${trackerFormat.trackerFormatId}"/>" title="Edit"><i class="fa fa-edit"></i></a> 
													<!--  <a href="#"title="Delete"><i class="fa fa-file-excel-o"></i></a>&nbsp;&nbsp;&nbsp; --> 
											</td>
												</tr>
											</c:forEach>
										</c:when>
										</c:choose>
									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
</html>
