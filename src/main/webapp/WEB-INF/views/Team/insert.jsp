<%--/**
 *
 * Author  		:Priyanga
 * Project 		: ServiceCart
 * Date    		: 04/07/2016
 * Description	: insert a new category
 *
 * #      Name         Version   Modified-Date      Description
 * -------------------------------------------------------------------------------------
 * 1   Priyanga      1.0        04/07/2016      Initial Creation
 * 2   Priyanga      2.0        04/07/2016       Modification
 *
 */
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
<script src="js/jquery-1.10.2.js"></script>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Team</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Add New Team</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Add New Team</h5>
							</div>
							<div class="ibox-content">
								<span style="color: red" id="message">${message}</span><br>
								<form:form class="form-horizontal" action="saveTeam" method="post" modelAttribute="team">
									<form:input type="hidden" id="teamId" value="" name="teamId" path="teamId"/>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Team Name</label>
										<div class="col-lg-8">
											<form:input type="text" placeholder="Team Name" class="form-control" name="teamName" id="teamName" path="teamName" />
											<span id="teamErr" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label required col-sm-4" for="email">Email:</label>
										<div class="col-sm-8">
											<form:input type="email" class="form-control" id="emailAddress" name="userFk.emailAddress" placeholder="Email Address" path="userFk.emailAddress"/> <span id="userErr" style="color: red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label required col-sm-4" for="pwd">Password:</label>
										<div class="col-sm-8">
											<form:input type="password" class="form-control" id="password" name="userFk.password" placeholder="Enter password" path="userFk.password"/> <span id="passErr" style="color: red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Status</label>
										<div class="col-lg-8">
											<label><form:radiobutton name="isActive" id="isactive" value="1" path="isActive"  checked="checked" /> Active</label> &nbsp; &nbsp; 
											<label><form:radiobutton  name="isActive" id="isactive" value="2" path="isActive"/> InActive</label>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="form-group">
										<label class="col-lg-4 control-label">&nbsp;</label>
										<div class="col-lg-8">
											<a href="cancelTeam" class="btn btn-white"><strong>Cancel</strong></a> &nbsp; &nbsp;
											<input type="submit" class="btn btn-primary" id="addTeam" value="Save">
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>		
	<script type="text/javascript">
	$(document).ready(function(){
		$("#addTeam").click(function(){
			
			
			var testemail=false;
			var teamName = $("#teamName").val();
			var email = $("#emailAddress").val();
			var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			var checkEmail = filter.test(email);
			var password = $("#password").val();
		
			
			if(teamName == null || teamName =="")
				{
				$("#passErr").text("");$("#userErr").text("");
				$("#teamErr").text("Team Name required");return false;}
			
			else if(email == null || email =="")
			{	$("#passErr").text("");$("#teamErr").text("");
				$("#userErr").text("Email Address required");return false;}
			
			else if(email!=null && email !="" && !checkEmail){
				$("#passErr").text("");$("#teamErr").text("");
				$("#userErr").text("Invalid Email Address");return false;
			}
			else if(password == null || password =="")
			{	$("#userErr").text("");$("#teamErr").text("");
				$("#passErr").text("Password required");return false;
			}else{
				$.ajax({
		        	type: 'POST',
		        	async: false,
		        	url: "signIn",
		        	data: {
		        		emailAddress: email,
		        		password : password
		        	},
		        	success: function(result){
		        		 if(result !=null && result != "" && result!='false'){ $("#userErr").text(" Email Address Already exixts"); testemail =false;}
		        		 else  testemail=true;
		            }
		        });
			}
		if(testemail){ $("#userErr").text(""); return true;}
		else {
			return false;}
			
		});
	});
	</script>
</body>
</html>
