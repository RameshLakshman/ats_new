
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Manage Questionnaire</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Manage Questionnaire</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span style="color: red">${message}</span>
								<div class="ibox-tools">
									<!--  <a href="addQues" class="btn btn-primary btn-xs"><i
										class="fa fa-pencil"></i> Add New</a>  -->
								</div>
							</div>
							<div class="ibox-content">
								<table
									class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>Questionnaire Name</th>
											<th>Tracker Id</th>
									        <th>Created Date</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${allQues != null}">
											<c:forEach items="${allQues}" var="allQues">
												<tr>
													<td><c:out value="${allQues.trackerQuestionnaireId}" /></td>
													<td><c:out value="${allQues.field}" /></td>
													<td><c:out value="${allQues.trackerFk.trackerId}" /></td>
													<td><c:out value="${allQues.rowCreated}" /></td>
													
									
													<td>
													<a href="editQues?questionnaire_id=<c:out value="${allQues.trackerQuestionnaireId}"/>"title="Edit"><i class="fa fa-edit"></i></a>	&nbsp;&nbsp;&nbsp; 
													<%-- <a href="deleteQues?questionnaire_id=<c:out value="${allQues.trackerQuestionnaireId}"/>"title="Delete"><i class="fa fa-file-excel-o"></i></a>&nbsp;&nbsp;&nbsp;  --%>
											
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
</html>
