
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ServiceCart</title>
</head>

<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Questionnaire</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Edit Questionnaire</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Edit Questionnaire</h5>
							</div>
							<div class="ibox-content">
								<span style="color: red" id="message">${message}</span><br>
								<form class="form-horizontal" action="updateQues" method="post">
									<div class="form-group">
										<label class="col-lg-4 control-label required">Questionnaire
											Id</label>
										<div class="col-lg-8">
											<input type="text" name="trackerQuestionnaireId" id="trackerQuestionnaireId"
												value="${questionnaire.trackerQuestionnaireId}" class="form-control"
												readonly="readonly">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Questionnaire
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Questionnaire Name"
												class="form-control" required="required" name="field"
												id="field" value="${questionnaire.field}">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Existing Trackers
											</label>
										<div class="col-lg-8">
											<select class="form-control m-b" id="trackerFk" 
												name="trackerFk" required="required">
												<option value="">Select Trackers</option>
												<c:if test="${trackerList != null}">
													<c:forEach items="${trackerList}" var="trackerList">
														<%-- <c:if test="${trackerList.isActive == 1}"> --%>
															<c:choose>
																<c:when
																	test="${trackerList.trackerId == questionnaire.trackerFk.trackerId}">
																	<option value="${trackerList.trackerId}"
																		selected="selected">${trackerList.trackerId}</option>
																</c:when>
																<c:otherwise>
																	<option value="${trackerList.trackerId}">${trackerList.trackerId}</option>
																</c:otherwise>
															</c:choose>
														<%-- </c:if> --%>
													</c:forEach>
												</c:if>

											</select>
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Status</label>
										<div class="col-lg-8">
											<c:if test="${questionnaire.is_active == 1 }">
												<label><input type="radio" name="is_active"
													id="is_active" value="1" checked="checked"> Active</label> &nbsp; &nbsp;
										<label><input type="radio" name="is_active"
													id="is_active" value="0"> InActive</label>
											</c:if>
											<c:if test="${questionnaire.is_active == 0 }">
												<label><input type="radio" name="is_active"
													id="is_active" value="1"> Active</label> &nbsp; &nbsp;
										<label><input type="radio" name="is_active"
													id="is_active" value="0" checked="checked"> InActive</label>
											</c:if>
										</div>
									</div>
<span id="quesErr" style="color: red;"></span>
									<div class="clearfix">&nbsp;</div>
									<div class="form-group">
										<label class="col-lg-4 control-label">&nbsp;</label>
										<div class="col-lg-8">
											<a href="quesList" class="btn btn-white"><strong>Cancel</strong></a>
											&nbsp; &nbsp;
											<button type="submit" class="btn btn-primary" id="submit-questionnaire">
												<strong>Save</strong>
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	
</body>
</html>
