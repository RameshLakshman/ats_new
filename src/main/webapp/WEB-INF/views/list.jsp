<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
	<title>Employee Management</title>
</head>
<body>
<a href="adminPage">Dashboard</a>
	<table id="listOfEmployees" border="1">
	<tr>
	    <td>ID</td>
	    <td>First Name</td>
	    <td>Last Name</td>
	    <td>Email</td>
	  </tr>
	<c:forEach items="${allEmployees}" var="employee">    
	  <tr>
	    <td>${employee.id}</td>
	    <td>${employee.firstName}</td>
	    <td>${employee.lastName}</td>
	    <td>${employee.email}</td>
	  </tr>
	</c:forEach>
	</table>
</body>
</html>