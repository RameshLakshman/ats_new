<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="css/4-col-portfolio.css" rel="stylesheet">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<style type="text/css">
.form-text {
	width: 37% !important;
	font-size: 12px;
}

.form-from-to {
	width: 37% !important;
}

.boxed {
	border: 2px solid grey;
	width: 955px;
	padding: 25px;
	margin: 25px;
	text-align: center;
}
</style>
<title>ATS</title>
</head>
<body>
	<div class="row">
		<div class="boxed col-md-3 col-sm-8">
		<a href="createCandidate" >Add New Candidate</a>
		
		 <table>                
          <tr>
             <th/>
             <th>ID</th>
             <th>Name</th>
             <th>Mail Id</th>
             <th>Mobile Number</th>
               <th>Current Company</th>
                 <th>CTC</th>
                 <th>NoticePeriod</th>
                 <th>Resume Status</th>
          </tr> 
<c:forEach var="i" items="${candidateList}" >

<tr>
			 <td>${i.candidate_id}</td>
             <td >${i.name}</td>
              <td >${i.mailId}</td>
              <td >${i.mobileNumber}</td>
             <td >${i.currentCompany}</td>
             <td >${i.ctc}</td>
             <td>${i.noticePeriod}</td>
             
          </tr>
</c:forEach>
</table>
		</div>
	</div>
</body>

<!-- <SCRIPT language="javascript">
$(function(){

	// add multiple select / deselect functionality
	$("#selectall").click(function () {
		  $('.case').attr('checked', this.checked);
		  alert($('input[name="case"]:checked'));
	});

	// if all checkbox are selected, check the selectall checkbox
	// and viceversa
	$(".case").click(function(){

		var checkedValues = $('input:checkbox:checked').map(function() {
		    return this.value;
		}).get();

		alert(checkedValues);

		
		  var parentValueToPush = [];
		   $('input:checkbox:checked').each(function() {
		    parentValueToPush.push($(this).val());

		         });
		   alert(parentValueToPush) ;

		
		/*  $.ajax({
	         type: "POST",
	         url: "/dowloadCandidateDetails",
	         data: {
	             myArray: checkedValues //notice that "myArray" matches the value for @RequestParam
                 //on the Java side
  },
	         success: function(msg){
	                     alert( "Data Saved: " + msg );
	                  }
	    });  */
		
		
		if($(".case").length == $(".case:checked").length) {
			$("#selectall").attr("checked", "checked");
			alert($('input[name="case"]:checked'));
		} else {
			$("#selectall").removeAttr("checked");
		}



		

	});




	 
});
</SCRIPT> -->
</html>