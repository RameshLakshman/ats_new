<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ATS</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/4-col-portfolio.css" rel="stylesheet">

    <style type="text/css">
    .form-text{
    width: 37% !important;
    font-size: 12px;
    }
    .form-from-to{
    width: 37% !important;
    
    }
     .boxed {
   border: 2px solid grey ;
   width: 150px;padding: 25px;margin: 25px;text-align: center;
   }
    </style>

</head>

<body>

<!--     Navigation
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            Brand and toggle get grouped for better mobile display
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Start Bootstrap</a>
            </div>
            Collect the nav links, forms, and other content for toggling
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Services</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </div>
            /.navbar-collapse
        </div>
        /.container
    </nav> -->

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">TRACKERS
                     <a href="javascript:void(0)" data-toggle="modal" data-target="#trackerModal" id="addTracker" class="access_link"><small>Add New Tracker</small></a>
                <br>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#searchTracker" id="access_link_search" class="access_link"><small>Search Trackers</small></a>
                </h1>
            </div>
        </div>
        <!-- /.row -->

        <!-- Projects Row -->
        <div class="row">
            <!-- <div class="col-md-3 portfolio-item"> -->
            <div>
                <div id="searchErrors" style="color: black;display: none;">No results found.</div>
           
               <a href="candidateList"> 
                <div id="ajaxSearchResponse" style="display: none;">
                 </div>
               </a>
                 
                 
        <div id="allListvalues">
                <a href="candidateList">
                    <!-- <img class="img-responsive" src="http://placehold.it/750x450" alt=""> -->
                     <c:forEach items="${allTrackers}" var="trackers">
					<div class="boxed col-md-3 col-sm-6">						
						<tr>
						    <td><c:out value="${trackers.clientFk.client_name}" />,<c:out value="${trackers.location}" /></td><br>
							<td><c:out value="${trackers.primarySkills}" /></td><br>
							<td><c:out value="${trackers.experienceFrom}" /> - <c:out value="${trackers.experienceTo}" /> Years</td><br>
							<td><c:out value="${trackers.salaryFrom}" /> - <c:out value="${trackers.salaryTo}" /> LPA</td>
						</tr>
					</div>	
				  </c:forEach>
               </a>
           </div>
            </div>
           
            </div>
        <hr>

        <!-- <!-- Pagination -->
       <!--  <div class="row text-center">
            <div class="col-lg-12">
                <ul class="pagination">
                    <li>
                        <a href="#">&laquo;</a>
                    </li>
                    <li class="active">
                        <a href="#">1</a>
                    </li>
                    <li>
                        <a href="#">2</a>
                    </li>
                    <li>
                        <a href="#">3</a>
                    </li>
                    <li>
                        <a href="#">4</a>
                    </li>
                    <li>
                        <a href="#">5</a>
                    </li>
                    <li>
                        <a href="#">&raquo;</a>
                    </li>
                </ul>
            </div> -->
        </div> 
        <!-- /.row -->

        <hr>
        <!-- Footer -->
        <!-- <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; Your Website 2014</p>
                </div>
            </div>
            /.row
        </footer> -->

    <!-- /.container -->


<!-- ADD TRACKER MODAL POPUP -->
	<div id="trackerModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Create New Tracker</h4>
				</div>
				<div class="modal-body">
				 <form role="form">
					<div class="form-group">
						<!-- <input type="text" placeholder="Existing Client" name="existsClient" id="existsClient" required="required" class="form-control form-text"> -->
						<select class="form-control m-b form-text pull-left" id="existsClient" name="existsClient">
						  <option value="">Existing Client</option>
						  <c:if test="${allClients != null}">
						  <c:forEach items="${allClients}" var="client">
						  <option value="${client.client_id}">${client.client_name}</option>
						  </c:forEach>
						  </c:if>
						</select>
						
					
					<input type="button" id="newClient" data-toggle="modal" data-target="#addClient" value="Create New Client" class="pull-right">
					</div>
					<div class="clearfix">&nbsp;</div>
					<div class="clearfix">&nbsp;</div>
					<div class="form-group">
						<input type="text" placeholder="Enter Primary Skill/Designation" name="primarySkill" id="primarySkill" required="required" class="form-control form-text pull-left">
						<input type="text" placeholder="Job Location" name="jobLocation" id="jobLocation" required="required" class="form-control form-text pull-right">
							<%-- <select class="form-control m-b form-text pull-right" id="jobLocation" name="jobLocation" disabled="disabled">
						 <option value="">Job Locations</option>
						  <c:if test="${clientLocations != null}">
						  <c:forEach items="${clientLocations}" var="clientLocation">
						  <option value="${clientLocation.client_id}">${clientLocation.client_name}</option>
						  </c:forEach>
						  </c:if>
						</select> --%>
					</div>
						<div class="clearfix">&nbsp;</div>
					<span id="primaryErr" style="color: red; font-size: 12px;" class="" ></span>
					<div class="clearfix">&nbsp;</div>
					<div class="form-group">
						<input type="text" placeholder="Additional Skill if any" name="additionalSkill" id="additionalSkill" required="required" class="form-control form-text pull-left">
						<span style="margin-left: 131px;"><small>(seperate each skill by , and either or by /)</small> </span>
					</div>
					 <div class="clearfix">&nbsp;</div>
					<div class="col-md-6">
					<label class="col-lg-0 control-label">Exp</label><br/>
					<div class="form-group">
						<input type="number" name="experienceFrom" id="experienceFrom" required="required" class="form-control form-from-to pull-left" min="0" value="0" onkeypress='return validateQty(event);'>
						 <input type="number" name="experienceTo" id="experienceTo" required="required" class="form-control form-from-to pull-right" min="0" value="0" onkeypress='return validateQty(event);'>
					</div>
					</div>
					<div class="col-md-6">
					<label class="col-lg-0 control-label">Salary band</label><br/>
					<div class="form-group">
						<input type="number" name="salaryFrom" id="salaryFrom" required="required" class="form-control form-from-to pull-left" min="0" value="0" onkeypress='return validateQty(event);'>
						 <input type="number" name="salaryTo" id="salaryTo" required="required" class="form-control form-from-to pull-right" min="0" value="0" onkeypress='return validateQty(event);'>
					</div>
					</div>
					<div class="clearfix"> &nbsp;</div>
					<div class="col-md-12">
					<label class="col-lg-0 control-label">Tracker Format <span style="color: red">*</span></label><br/>
					<div class="form-group">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#prevFormat"  class="btn btn-primary"><small>Previous format</small></a>
						<span style="margin-left: 154px;"><b>or</b></span>
						<a href="javascript:void(0);" data-toggle="modal" data-target="#newFormat" class="pull-right btn btn-primary"><small>Create New</small></a>
						 <span id="salErr" style="color: red;"></span>
					</div>
					</div>
					<div class="clearfix"> &nbsp;</div>
					<div class="col-md-12">
					<label class="col-lg-0 control-label">Questionnaire</label><br/>
					<div class="form-group">
						<a href="javascript:void(0)" data-toggle="modal" data-target="#prevQuestion" class="btn btn-primary"><small>Previous format</small></a>
						<span style="margin-left: 154px;"><b>or</b></span>
						<a href="javascript:void(0)" data-toggle="modal" data-target="#newQuestion" class="pull-right btn btn-primary"><small>Create New</small></a>
						 <span id="salErr" style="color: red;"></span>
					</div>
					</div>
					<div class="clearfix"> &nbsp;</div>
					<!-- <div class="form-group">
						<input type="text" placeholder="allot to team" name="teamAllot" id="teamAllot" class="form-control form-from-to">
						
					</div> -->
					<div class="clearfix">&nbsp;</div>
					<span id="trackerErr" style="color: red;"></span>
					<div class="clearfix">&nbsp;</div>
					<div class="col-md-12 center-align text-center">
						<button type="submit" class="btn btn-primary" id="submit-tracker">
												<strong>Save</strong>
											</button>
					</div>	
					</form>
					<div class="clearfix"></div>
				</div>
				
			</div>
		</div>
	</div>

<!-- To add new Client -->

<div id="addClient" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Create New Tracker</h4>
					<input type="hidden" id="checkFormat" value="">
					<input type="hidden" id="checkQuestion" value="">
				</div>
				<div class="modal-body">
				 <form role="form">
					<div class="form-group">
						<input type="text" placeholder="Enter Client Name" name="clientName" id="clientName" required="required" class="form-control form-text">
					</div>
						<span id="newClientErr" style="color: red; font-size: 12px;"></span>
					 <div class="clearfix">&nbsp;</div>
					<div class="form-group">
							<input type="text" placeholder="Location" name="location" id="location" required="required" class="form-control form-text">
						<%--  <c:choose>
						<c:when test="${locationList != null}">
						<select class="form-control m-b form-text pull-left" id="existsLocation" name="existsLocation"  multiple="multiple">
						<!-- <option value="">Choose location</option> -->
						</c:when>
						<c:otherwise>
						<select class="form-control m-b form-text pull-left" id="existsLocation" name="existsLocation" disabled="disabled">
						<option value="">No Locations Found</option>
						</c:otherwise>
						</c:choose>						
						  <c:if test="${locationList != null}">
						  <c:forEach items="${locationList}" var="location">
						  <option value="${location.locationId}">${location.location}</option>
						  </c:forEach>
						  </c:if> 
						</select><a href="#" style="font-size: 18px; margin-left: 5px;" data-toggle="modal" data-target="#addLocation" id="addNewLocation"><span><b>+</b></span></a> --%>
					</div>
					 <span id="locationErr" style="color: red; font-size: 12px;"></span>
						<div class="clearfix">&nbsp;</div>
					<div class="form-group">
						<input type="text" placeholder="Domain" name="domain" id="domain" required="required" class="form-control form-text pull-left">
						<input type="text" placeholder="URL" name="url" id="url" required="required" class="form-control form-text pull-right">
					</div>
					<div class="clearfix">&nbsp;</div>
					<span id="domain-url-err" style="color: red; font-size: 12px;"></span>
					 <div class="clearfix">&nbsp;</div> 
					<div class="form-group">
						<input type="text" placeholder="SPOC MAIL ID" name="spoc" id="spoc" required="required" class="form-control form-text">
						 <span id="additionalErr" style="color: red;"></span>
					</div>
							<span id="spocErr" style="color: red; font-size: 12px;"></span>							
					<div class="clearfix">&nbsp;</div>
					<span id="clientErr" style="color: red;"></span>
					<div class="col-md-12 center-align text-center">
						<button type="submit" class="btn btn-primary" id="submit-client">
												<strong>Save</strong>
											</button>
					</div>	
					</form>
					<div class="clearfix">&nbsp;</div>
				</div>
				
			</div>
		</div>
	</div>

<!-- SEARCH TRACKER MODAL POPUP -->
	<div id="searchTracker" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
				 <form role="form">
				
					<div class="form-group">
						<input type="text" placeholder="Client Name" name="searchclient" id="searchclient" required="required" class="form-control form-text">
						<span id="clientEr" style="color: red;"></span>
					</div>
					
					<div class="col-md-6">
					<label class="col-lg-0 control-label">Exp</label><br/>
					<div class="form-group">
						<input type="number" name="expFrom" id="expFrom" required="required" class="form-control form-from-to pull-left" min="0" value="0" onkeypress='return validateQty(event);'>
						 <input type="number" name="expTo" id="expTo" required="required" class="form-control form-from-to pull-right" min="0" value="0" onkeypress='return validateQty(event);'>
						
					</div>
					</div>
					<div class="clearfix">&nbsp;</div>
					<span id="expErr" style="color: red;"></span>
					<div class="clearfix">&nbsp;</div>
					<div class="form-group">
						<input type="text" placeholder="Skill" name="skill" id="skill" required="required" class="form-control form-text">
						<span id="skillErr" style="color: red;"></span>
					</div>
					<div class="clearfix">&nbsp;</div>
					<span id="searchhhErr" style="color: red;"></span>
					<div class="clearfix">&nbsp;</div>
					<div class="col-md-12 center-align text-center">
						<button type="submit" class="btn btn-primary" id="searchClient">
												<strong>Search</strong>
											</button>
											</div>
					</form>
					<div class="clearfix"></div>
				</div>
				</div>
			</div>
		</div>

<!--  PREVIOUS TRACKER FORMAT MODAL POPUP -->	
<c:if test="${prevFormats!=null }">	
<div id="prevFormat" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="height: 300px !important">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Previous Tracker Formats</h4>
					<div class="modal-body">
							<c:forEach items="${prevFormats}" var="prevFormat">
								<div class="dropdown">
									<button class="dropdown-toggle" data-toggle="dropdown" id="format_name">${prevFormat.key.trackerFormatName}<span class="caret"></span></button>&nbsp;&nbsp;
									<input type="radio" name="prevformat_radio" class="prevformat_radio" class="prevformat_checkbox" value="${prevFormat.key.id}">
									<ul class="dropdown-menu">
										<c:forEach items="${prevFormat.value}" var="format">
											<li>${format.field}</li>
										</c:forEach>
									</ul>
								</div>
								<br>
							</c:forEach>
						<br>
						<div class="form-group">
							<div class="col-sm-8">
							<span id="prevformatErr" style="color: red"></span>
							<img alt="" src="img/spinner.gif" style="margin-left: 215px;padding:10px;" class="spinner">
								<input type="submit" value="Save" id="prevformat_save" class="btn btn-primary" style="margin-left: 200px;" >
								<span id="salErr" style="color: red;"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>		
</c:if>
<!--  CREATE NEW TRACKER FORMAT MODAL POPUP -->				
	<div id="newFormat" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"  style="height: 500px !important">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">New Tracker Format</h4>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-sm-6">
								<input type="text" name="tfname" id="tfname" class="form-control" placeholder="Enter Tracker Name" >
							</div>
						</div>
						<div class="clearfix">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>&nbsp;&nbsp;
						<div class="form-group">
							<div class="col-sm-10">
								<label class="control-label col-sm-8" for="sno">S. No</label>
								<input type="checkbox" name="sno" id="sno" checked="checked" value="S. No" disabled="disabled">
								<span id="salErr" style="color: red;"></span>
							</div>
						</div>
						<div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-10">
								<label class="control-label col-sm-8" for="name">Name</label>
								<input type="checkbox" name="name" id="name" checked="checked" value="Name" disabled="disabled">
								<span id="salErr" style="color: red;"></span>
							</div>
						</div>
						<div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-10">
								<label class="control-label col-sm-8" for="mailId">Mail Id</label>
								<input type="checkbox" name="mailId" id="mailId" checked="checked" value="Mail Id" disabled="disabled">
								   
							</div>
						</div><div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-10">
								<label class="control-label col-sm-8" for="mobileNo">Mobile Number</label>
								<input type="checkbox" name="mobileNo" id="mobileNo" checked="checked" value="Mobile Number" disabled="disabled">
								
							</div>
						</div><div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-10">
								<label class="control-label col-sm-8" for="noticePeriod">Notice Period</label>
								<input type="checkbox" name="noticePeriod" id="noticePeriod" checked="checked" value="Notice Period" disabled="disabled">
								
							</div>
						</div><div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-10">
								<label class="control-label col-sm-8" for="ctc">CTC</label>
								<input type="checkbox" name="ctc" id="ctc" checked="checked" value="CTC" disabled="disabled">
								
							</div>
						</div>
						<div class="clearfix" id="addextra">&nbsp;</div>
						<div class="form-group">
							<div class="col-sm-8">
								<button style="cursor: auto;" class="btn btn-primary">Add Your Own</button>
								<a href="javascript:void(0)" data-toggle="modal" data-target="#addNewFormat" ><img alt="" src="img/plus.png"></a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-8">
								<span id="tfErr" style="color: red;"></span>
								<img alt="" src="img/spinner.gif" style="margin-left: 215px;padding:10px;" class="spinner">
								<input type="submit" value="Save" id="savenewFormat" class="btn btn-primary" style="margin-left: 200px;">
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>	
		</div>
		<!--  ADD TRACKER FORMAT MODAL POPUP -->				
	<div id="addNewFormat" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"  style="height: 500px !important">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Custom Field<span style="color: red">*</span></h4>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-sm-6">
								<input type="text" name="trackerField" id="trackerField" class="form-control" placeholder="Enter Tracker Format" >
							</div>
						</div>
						</div><div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-8">
								<span id="atfErr" style="color: red;"></span>
								<img alt="" src="img/spinner.gif" style="margin-left: 215px;padding:10px;" class="spinner">
								<input type="submit" value="Save" id="addExtraField" class="btn btn-primary" style="margin-left: 200px;">
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>	
		<!--  ADD TRACKER Question MODAL POPUP -->				
		
<!--  CREATE NEW Question MODAL POPUP -->				
	<div id="newQuestion" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"  style="height: 500px !important">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">New Questionnaire</h4>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-sm-6">
								<input type="text" name="questionName" id="questionName" class="form-control" placeholder="Enter Questionnaire Name" >
							</div>
						</div>
						<div class="clearfix">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>&nbsp;&nbsp;
						<div class="form-group">
							<div class="col-sm-12">
								<div>View Default Questions and add your own</div>
							</div>
						</div>
						<div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-12">
								<label class="control-label col-sm-10" for="name">What was your recent project and role in that?</label>
								<input type="checkbox" name="project" id="project" checked="checked" value="What was your recent project and role in that?" disabled="disabled">
								<span id="salErr" style="color: red;"></span>
							</div>
						</div>
						<div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-12">
								<label class="control-label col-sm-10" for="mailId">Worked on scale-able applications?</label>
								<input type="checkbox" name="application" id="application" checked="checked" value="Worked on scale-able applications?" disabled="disabled">
								   
							</div>
						</div><div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-12">
								<label class="control-label col-sm-10" for="mobileNo">Preferred job locations?</label>
								<input type="checkbox" name="locations" id="locations" checked="checked" value="Preferred job locations?" disabled="disabled">
								
							</div>
						</div><div class="clearfix"  id="cusQus">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-8">
								<button style="cursor: auto;" class="btn btn-primary">Add Your Own</button>
								<a href="javascript:void(0)" data-toggle="modal" data-target="#addNewQuestion" ><img alt="" src="img/plus.png"></a>
							</div>
						</div>
						</div><div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-8">
								<span id="questionErr" style="color: red;"></span>
								<img alt="" src="img/spinner.gif" style="margin-left: 215px;padding:10px;" class="spinner">
								<input type="submit" value="Save" id="savenewQuestion" class="btn btn-primary" style="margin-left: 200px;">
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>	
			<!--  PREVIOUS TRACKER QUESTION MODAL POPUP -->		
<c:if test="${prevQuestions!=null }">
<div id=prevQuestion class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="height: 300px !important">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Previous Tracker Questionnaire</h4>
					<div class="modal-body">
							<c:forEach items="${prevQuestions}" var="prevQus">
								<div class="dropdown">
									<button class="dropdown-toggle" data-toggle="dropdown" id="format_name">${prevQus.key.questionnaireName}<span class="caret"></span></button>&nbsp;&nbsp;
									<input type="radio" name="prevquestion_radio" class="prevquestion_radio" class="prevformat_checkbox" value="${prevQus.key.questionnaireId}">
									<ul class="dropdown-menu">
										<c:forEach items="${prevQus.value}" var="question">
											<li>${question.field}</li>
										</c:forEach>
									</ul>
								</div>
								<br>
							</c:forEach>
						
						<br>
						<div class="form-group">
							<div class="col-sm-8">
							<span id="prevquestionErr" style="color: red"></span>
							<img alt="" src="img/spinner.gif" style="margin-left: 215px;padding:10px;" class="spinner">
								<input type="submit" value="Save" id="prevquestions_save" class="btn btn-primary" style="margin-left: 200px;" >
								<span id="salErr" style="color: red;"></span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</c:if>
	<div id="addNewQuestion" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header"  style="height: 500px !important">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Custom Question<span style="color: red">*</span></h4>
					<div class="modal-body">
						<div class="form-group">
							<div class="col-sm-6">
								<input type="text" name="questionField" id="questionField" class="form-control" placeholder="Enter Question" >
							</div>
						</div>
						</div><div class="clearfix">&nbsp; </div>
						<div class="form-group">
							<div class="col-sm-8">
								<span id="addquestionErr" style="color: red;"></span>
								<img alt="" src="img/spinner.gif" style="margin-left: 215px;padding:10px;" class="spinner">
								<input type="submit" value="Save" id="addCusQus" class="btn btn-primary" style="margin-left: 200px;">
								
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>	

    <!-- jQuery -->
    <script src="js/jquery-1.10.2.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- For Trackers -->
    <script src="js/trackers.js"></script>
    
</body>

</html>
