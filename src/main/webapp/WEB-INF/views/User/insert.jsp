<%--/**
 *
 * Author  		:Priyanga
 * Project 		: ServiceCart
 * Date    		: 04/07/2016
 * Description	: insert a new category
 *
 * #      Name         Version   Modified-Date      Description
 * -------------------------------------------------------------------------------------
 * 1   Priyanga      1.0        04/07/2016      Initial Creation
 * 2   Priyanga      2.0        04/07/2016       Modification
 *
 */
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
<script src="js/jquery-1.10.2.js"></script>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>User</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Add New User</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Add New User</h5>
							</div>
							
							<div class="ibox-content">
							
								<span style="color: red" id="message">${message}</span><br>
								
								<form:form class="form-horizontal" action="saveUser" method="post" modelAttribute="user">
								
									<form:input type="hidden" id="userId" value="" name="userId" path="userId"/>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Email Address</label>
										<div class="col-lg-8">
											<form:input type="text" placeholder="Email Address" class="form-control" name="emailAddress" id="emailAddress" path="emailAddress" />
											<span id="userErr" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Password</label>
										<div class="col-lg-8">
											<form:input type="password" placeholder="Password" class="form-control" name="password" id="password" path="password" />
											<span id="passErr" style="color:red"></span>
										</div>
									</div>
									 <div class="form-group">
										<label class="col-lg-4 control-label required">Team</label>
										<div class="col-lg-8">
											<select name="teams" class="form-control" id="teams">
											<option value="">Select Team</option>
											<c:forEach items="${teamList}" var="team">
											<option value="${team.teamId}"> ${team.teamName} </option>
											</c:forEach>
											</select>
											<span id="teamErr" style="color:red"></span>
										</div>
									</div>
									 
									<div class="form-group">
										<label class="col-lg-4 control-label required">Status</label>
										<div class="col-lg-8">
											<label><form:radiobutton name="isActive" id="isactive" value="1" path="isActive"  checked="checked" /> Active</label> &nbsp; &nbsp; 
											<label><form:radiobutton  name="isActive" id="isactive" value="2" path="isActive"/> InActive</label>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="form-group">
										<label class="col-lg-4 control-label">&nbsp;</label>
										<div class="col-lg-8">
											<a href="cancelUser" class="btn btn-white"><strong>Cancel</strong></a> &nbsp; &nbsp;
											<input type="submit" class="btn btn-primary" id="saveUser" value="Save">
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>		
	<script type="text/javascript">
	$(document).ready(function(){
		$("#saveUser").click(function(){
			var email = $("#emailAddress").val();
			var password = $("#password").val();
			var team = $("#teams").val();
			var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			if(email == null || email =="")
				{$("#passErr").text("");$("#teamErr").text(""); $("#userErr").text("Email Address required"); return false;}
			else if(email != null && email !="" && !filter.test(email))
				{$("#passErr").text("");$("#teamErr").text(""); $("#userErr").text("Invalid Email Address"); return false;}	
			else if(password == null || password == "")
				{$("#userErr").text(""); $("#teamErr").text(""); $("#passErr").text("Password required"); return false;}
			else if(team == null || team == "")
			{$("#passErr").text(""); $("#userErr").text(""); $("#teamErr").text("Please Select Team"); return false;}
		});
	});
	</script>
</body>
</html>
