<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>User</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>User</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span style="color: #18a689">${message}</span>
								<div class="ibox-tools">
								<a href="addUser" class="btn btn-primary btn-xs"><i
										class="fa fa-pencil"></i> Add New</a>
								</div>
							</div>
							<div class="ibox-content">
								<table
									class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>Email Address</th>
											<th>Team</th>
											<th>Date Created</th>
											<th>Is Active</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<c:choose>
										<c:when test="${tUser != null}">
											<c:forEach items="${tUser}" var="tuser">
												<tr>
												<td><c:out value="${tuser.userFk.userId}" /></td>
													<td><c:out value="${tuser.userFk.emailAddress}" /></td>
													<td><c:out value="${tuser.teamFk.teamName}"/></td>
													<td><c:out value="${tuser.userFk.rowCreated}" /></td>
													<td><c:out value="${tuser.userFk.isActive}" /></td>
													<td>
													<a href="editUser?userId=<c:out value="${tuser.userFk.userId}"/>"title="Edit"><i class="fa fa-edit"></i></a>	&nbsp;&nbsp;&nbsp; 
													<a href="deleteUSer?userId=<c:out value="${tuser.userFk.userId}"/>"title="Delete"><i class="fa fa-file-excel-o"></i></a>&nbsp;&nbsp;&nbsp;
											
												</tr>
											</c:forEach>
										</c:when>
										</c:choose>
									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	</body>
</html>
