
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ServiceCart</title>
</head>

<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Client</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Edit Client</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Edit Client</h5>
							</div>
							<div class="ibox-content">
								<span style="color: red" id="message">${message}</span><br>
								<form class="form-horizontal" action="updateClient" method="post">
									<div class="form-group">
										<label class="col-lg-4 control-label required">Client
											Id</label>
										<div class="col-lg-8">
											<input type="text" name="client_id" id="client_id"
												value="${client.client_id}" class="form-control"
												readonly="readonly">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Client
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Client Name"
												class="form-control" required="required" name="client_name"
												id="client_name" value="${client.client_name}">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Domain
											</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Domain"
												class="form-control" required="required" name="domain"
												id="domain" value="${client.domain}">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Mail Id
											</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Mail Id"
												class="form-control" required="required" name="mail_id"
												id="mail_id" value="${client.mail_id}">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Client Url
											</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Client Url"
												class="form-control" required="required" name="client_url"
												id="client_url" value="${client.client_url}">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									<%-- <div class="form-group">
										<label class="col-lg-4 control-label required">Location
											</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Location"
												class="form-control" required="required" name="location"
												id="location" value="${client.location}">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div> --%>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Spoc
											</label>
										<div class="col-lg-8">
											<input type="text" placeholder="spoc"
												class="form-control" required="required" name="spoc"
												id="spoc" value="${client.spoc}">
											<span id="err_msg" style="color:red"></span>
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Status</label>
										<div class="col-lg-8">
											<c:if test="${client.is_active == 1 }">
												<label><input type="radio" name="is_active"
													id="is_active" value="1" checked="checked"> Active</label> &nbsp; &nbsp;
										<label><input type="radio" name="is_active"
													id="is_active" value="0"> InActive</label>
											</c:if>
											<c:if test="${client.is_active == 0 }">
												<label><input type="radio" name="is_active"
													id="is_active" value="1"> Active</label> &nbsp; &nbsp;
										<label><input type="radio" name="is_active"
													id="is_active" value="0" checked="checked"> InActive</label>
											</c:if>
										</div>
									</div>
	<span id="clientErr" style="color: red;">&nbsp;</span>

									<div class="clearfix">&nbsp;</div>
									<div class="form-group">
										<label class="col-lg-4 control-label">&nbsp;</label>
										<div class="col-lg-8">
											<a href="clientList" class="btn btn-white"><strong>Cancel</strong></a>
											&nbsp; &nbsp;
											<button type="submit" class="btn btn-primary" id="add-new-client">
												<strong>Save</strong>
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	
</body>
</html>
