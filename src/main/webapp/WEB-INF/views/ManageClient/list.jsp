
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Manage Clients</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Manage Clients</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span style="color: red">${message}</span>
								<div class="ibox-tools">
									 <a href="addclient" class="btn btn-primary btn-xs"><i
										class="fa fa-pencil"></i> Add New</a> 
								</div>
							</div>
							<div class="ibox-content">
								<table
									class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											<th>Client Name</th>
											<th>Domain</th>
											<!-- <th>Location</th> -->
											<th>Email</th>
											<th>Url</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${allClients != null}">
											<c:forEach items="${allClients}" var="allClients">
												<tr>
													<td><c:out value="${allClients.client_id}" /></td>
													<td><c:out value="${allClients.client_name}" /></td>
													<td><c:out value="${allClients.domain}" /></td>
													<%-- <td><c:out value="${allClients.location}" /></td> --%>
													<td><c:out value="${allClients.mail_id}" /></td>
													<td><c:out value="${allClients.client_url}" /></td>
													
									
													<td>
													<a href="editClient?client_id=<c:out value="${allClients.client_id}"/>"title="Edit"><i class="fa fa-edit"></i></a>	&nbsp;&nbsp;&nbsp; 
													<%-- <a href="deleteClient?client_id=<c:out value="${allClients.client_id}"/>"title="Delete"><i class="fa fa-file-excel-o"></i></a>&nbsp;&nbsp;&nbsp;  --%>
											
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
</html>
