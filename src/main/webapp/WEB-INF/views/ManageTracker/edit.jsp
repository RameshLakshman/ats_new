<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">

 .form-from-to{
    width: 22% !important;
    
    }

</style>
</head>
<body>
<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Tracker</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Edit Tracker</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Edit Tracker</h5>
							</div>
							<div class="ibox-content">
								<span style="color: red" id="message">${message}</span><br>
								<form class="form-horizontal" action="editTracker" method="post">
									<input type="hidden" id="categoryId" value="">
									<div class="form-group">
										<label class="col-lg-4 control-label required">Existing Client
											Name</label>
										<div class="col-lg-8">
											 <!-- <input type="text" placeholder="Existing Client"
												class="form-control" name="existsClient"
												id="existsClient">  -->
												<select class="form-control m-b" id="existsClient" name="existsClient">
												<c:choose>
												<c:when test="${client != null}">
												<option value="${client.client_id}" selected="selected">${client.client_name}</option>
												</c:when>
												<c:otherwise>
												<option value="">Existing Client</option>
												</c:otherwise>
												</c:choose>
													  <c:if test="${allClients != null}">
													  <c:forEach items="${allClients}" var="clients">
													  <c:if test="${client.client_id != clients.client_id}">
													  <option value="${clients.client_id}">${clients.client_name}</option>
													  </c:if>
													  </c:forEach>
													  </c:if>
												</select>
																								
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Enter Primary Skill/Deisgnation
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Primary Skill/Deisgnation"
												class="form-control"  name="primarySkill" value="${tracker.primarySkills}"
												id="primarySkill">
												<input type="hidden" name="tracker_id" value="${tracker.trackerId}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Job Location
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Job Location"
												class="form-control"  name="location" value="${tracker.location}"
												id="jobLocation">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Additional Skills if any
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Additional Skill"
												class="form-control"  name="additionalSkill"
												id="additionalSkill" value="${tracker.additionalSkills}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Exp
											</label>
										<div class="col-lg-8">
											<input type="number"
												class="form-control form-from-to pull-left"  name="experienceFrom"
												id="experienceFrom" value="${tracker.experienceFrom}">
												<input type="number"
												class="form-control form-from-to"  name="experienceTo"
												id="experienceTo" value="${tracker.experienceTo}">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Salary
											</label>
										<div class="col-lg-8">
											<input type="number"
												class="form-control form-from-to pull-left"  name="salaryFrom"
												id="salaryFrom" value="${tracker.salaryFrom}">
												<input type="number"
												class="form-control form-from-to"  name="salaryTo"
												id="salaryTo" value="${tracker.salaryTo}">
											
										</div>
									</div>
									<span id="trackerErr" style="color: red;">&nbsp;</span>
									<div class="clearfix">&nbsp;</div>
									<div class="form-group">
										<label class="col-lg-4 control-label">&nbsp;</label>
										<div class="col-lg-8">
											<a href="cancel" class="btn btn-white"><strong>Cancel</strong></a>
											&nbsp; &nbsp;
											<button type="submit" class="btn btn-primary"
												id="save-tracker">
												<strong>Save</strong>
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	
</body>
</html>