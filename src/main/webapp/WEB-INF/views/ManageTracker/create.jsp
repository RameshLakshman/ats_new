<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">

 .form-from-to{
    width: 17% !important;
    
    }

</style>
</head>
<body>
<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Tracker</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Add New Tracker</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Add New Tracker</h5>
							</div>
							<div class="ibox-content">
								<span style="color: red" id="message">${message}</span><br>
								<form class="form-horizontal" action="addTracker" method="post">
									<input type="hidden" id="categoryId" value="">
									<div class="form-group">
										<label class="col-lg-4 control-label required">Existing Client
											Name</label>
										<div class="col-lg-8">
											 <!-- <input type="text" placeholder="Existing Client"
												class="form-control" name="existsClient"
												id="existsClient">  -->
												<select class="form-control m-b" id="existsClient" name="existsClient">
													  <option value="">Existing Client</option>
													  <c:if test="${allClients != null}">
													  <c:forEach items="${allClients}" var="client">
													  <option value="${client.client_id}">${client.client_name}</option>
													  </c:forEach>
													  </c:if>
												</select>
																								
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label required">Enter Primary Skill/Deisgnation
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Primary Skill/Deisgnation"
												class="form-control"  name="primarySkill"
												id="primarySkill">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Job Location
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Job Location"
												class="form-control"  name="location"
												id="jobLocation">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Additional Skills if any
											Name</label>
										<div class="col-lg-8">
											<input type="text" placeholder="Additional Skill"
												class="form-control"  name="additionalSkill"
												id="additionalSkill">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Exp
											</label>
										<div class="col-lg-8">
											<input type="number"
												class="form-control form-from-to pull-left"  name="experienceFrom"
												id="experienceFrom" value="0">
												<input type="number"
												class="form-control form-from-to"  name="experienceTo"
												id="experienceTo" value="0">
											
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Salary
											</label>
										<div class="col-lg-8">
											<input type="number"
												class="form-control form-from-to pull-left"  name="salaryFrom"
												id="salaryFrom" value="0">
												<input type="number"
												class="form-control form-from-to"  name="salaryTo"
												id="salaryTo" value="0">
											
										</div>
									</div>
									
									
									
									<span id="trackerErr" style="color: red;">sddsfc</span>
									
									<div class="clearfix">&nbsp;</div>
									
									<div class="form-group">
										<label class="col-lg-4 control-label">&nbsp;</label>
										<div class="col-lg-8">
											<a href="cancel" class="btn btn-white"><strong>Cancel</strong></a>
											&nbsp; &nbsp;
											<button type="submit" id="save-tracker" class="btn btn-primary">
												<strong>Save</strong>
											</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	
</body>
</html>