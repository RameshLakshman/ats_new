
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Manage Trackers</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Manage Trackers</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<span style="color: red">${message}</span>
								<div class="ibox-tools">
								<!-- <a href="addTracker" class="btn btn-primary btn-xs"><i
										class="fa fa-pencil"></i> Add New</a> -->
									<!-- <a href="addStatus" class="btn btn-primary btn-xs"><i
										class="fa fa-pencil"></i> Add New</a> -->
								</div>
							</div>
							<div class="ibox-content">
								<table
									class="table table-striped table-bordered table-hover dataTables-example">
									<thead>
										<tr>
											<th>ID</th>
											 <th>Client</th> 
											<th>Primary skill</th>
											<th>Location</th>
											<th>Additional skill</th>
											<th>Experience</th>
											<th>CTC</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
									<c:choose>
										<c:when test="${allTrackers != null}">
											<c:forEach items="${allTrackers}" var="trackers">
												<tr>
												<td><c:out value="${trackers.trackerId}" /></td>
													 <td><c:out value="${trackers.clientFk.client_name}" /></td> 
													<td><c:out value="${trackers.primarySkills}" /></td>
													<td><c:out value="${trackers.location}" /></td>
													<td><c:out value="${trackers.additionalSkills}" /></td>
													<td><c:out value="${trackers.experienceFrom}" /> to <c:out value="${trackers.experienceTo}" /> years</td>
													<td><c:out value="${trackers.salaryFrom}" /> to <c:out value="${trackers.salaryTo}" /></td>
													<td>
													<a href="editTracker?tracker_id=<c:out value="${trackers.trackerId}"/>"title="Edit"><i class="fa fa-edit"></i></a>	&nbsp;&nbsp;&nbsp; 
													<%--  <a href="deleteTracker?tracker_id=<c:out value="${trackers.trackerId}"/>"title="Delete" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-file-excel-o"></i></a>&nbsp;&nbsp;&nbsp; --%> 
													 <a href="trackerFormat?tracker_id=<c:out value="${trackers.trackerId}"/>"title="View Tracker Format"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;
													 <a href="getQuestionnaireByTracker?tracker_id=<c:out value="${trackers.trackerId}"/>"title="View Questionnaire"><i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;
											</td>
												</tr>
											</c:forEach>
										</c:when>
										</c:choose>
									</tbody>
								</table>

							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
</html>
