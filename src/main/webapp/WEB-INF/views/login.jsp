<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>ATS</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container" id="signIn">
  <h2>Login</h2>
  <form:form class="form-horizontal" role="form" action="signIn" method="post">
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-3">
        <input type="email" class="form-control" id="emailAddress" name="emailAddress" placeholder="Email Address"/>
        <span id="userErr" style="color: red"></span>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-3">
        <input type="password" class="form-control" id="password" name="password" placeholder="Enter password"/>
        <span id="passErr" style="color: red"></span>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <span id="loginErr" style="color: red;"></span>
      </div>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <input type="submit" class="btn btn-default" id="login" value="Login">
      </div>
    </div>
  </form:form>
</div>


<%-- <div class="container" id="signUp" style="display: none;" >
  <h2>SignUp</h2>
  
  <form:form class="form-horizontal" role="form" action="signUp" method="post" modelAttribute="user">
    <div class="form-group">
      <label class="control-label col-sm-2" for="username">UserName:</label>
      <div class="col-sm-3">
        <input type="text" class="form-control" id="username" name="username" placeholder="UserName">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-3">
        <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
      </div>
    </div>
     <div class="form-group">
      <label class="control-label col-sm-2" for="pwd">Password:</label>
      <div class="col-sm-3">
        <input type="password" class="form-control" id="password" name="pasword" placeholder="Enter password">
      </div>
    </div>
   <div class="form-group">
      <a href="javascript:void(0);" id="loginLink">Go to Login</a>
    </div>
    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form:form>
</div> --%>



<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>		
<script type="text/javascript">
	$(document).ready(function(){
		$("#login").click(function(){
			var login = false;
			var email = $("#emailAddress").val();
			var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			var checkEmail = filter.test(email);
			var password = $("#password").val();
			if(email == null || email =="")
			{	$("#passErr").text("");
				$("#userErr").text("Email Address required");}
			
			else if(email!=null && email !="" && !checkEmail){
				$("#passErr").text("");
				$("#userErr").text("Invalid Email Address");
			}
			else if(password == null || password =="")
			{	$("#userErr").text("");
				$("#passErr").text("Password required");}
			else{
			$.ajax({
	        	type: 'POST',
	        	async: false,
	        	url: "signIn",
	        	data: {
	        		emailAddress: email,
	        		password : password
	        	},
	        	success: function(result){
	        			if(result != 'false'){
	        				window.location.href=result;
	        			}
	        			 else{
	        				$("#passErr").text("");	$("#userErr").text("");
	        				$("#loginErr").text("Incorrect Email Address/ Password");
	        				login = false;
	        			} 
	            }
	        });
			}
		if(login)
			window.location.href = "adminPage";
		else{ 
			return false;}
	return false;
		});
	});
	</script>
</body>
</html>

