<%--/**
 *
 * Author  		:Priyanga
 * Project 		: ServiceCart
 * Date    		: 04/07/2016
 * Description	: insert a new category
 *
 * #      Name         Version   Modified-Date      Description
 * -------------------------------------------------------------------------------------
 * 1   Priyanga      1.0        04/07/2016      Initial Creation
 * 2   Priyanga      2.0        04/07/2016       Modification
 *
 */
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ATS</title>
<script src="js/jquery-1.10.2.js"></script>
</head>
<body>
	<div id="wrapper">
		<jsp:include page="../layout/sidebar.jsp" />
		<div id="page-wrapper" class="gray-bg">
			<jsp:include page="../layout/topmenu.jsp" />
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>Role</h2>
					<ol class="breadcrumb">
						<li><a href="adminPage">Home</a></li>
						<li class="active"><strong>Edit Role</strong></li>
					</ol>
				</div>
				<div class="col-lg-2"></div>
			</div>
			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-2">&nbsp;</div>
					<div class="col-lg-8">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>Edit Role</h5>
							</div>
							<div class="ibox-content">
								<span style="color: red" id="message">${message}</span><br>
								<form:form class="form-horizontal" action="saveRole" method="post" modelAttribute="role">
								
								<div class="form-group">
										<label class="col-lg-4 control-label required">Role Id</label>
										<div class="col-lg-8">
											<form:input placeholder=""  name="id" id="id" path="id"  readonly="true" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Authority</label>
										<div class="col-lg-8">
											<form:input type="text" placeholder="Authority" class="form-control" name="authority" id="authority" path="authority" />
											<span id="authorityErr" style="color:red"></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-lg-4 control-label required">Status</label>
										<div class="col-lg-8">
											<c:if test="${role.isActive == 1 }">
												<label><form:radiobutton name="isActive" id="isactive" value="1" path="isActive"  checked="checked" /> Active</label> &nbsp; &nbsp;
												<label><form:radiobutton  name="isActive" id="isactive" value="2" path="isActive"/> InActive</label>
											</c:if>
											<c:if test="${role.isActive == 0}">
												<label><form:radiobutton name="isActive" id="isactive" value="1" path="isActive"/> Active</label> &nbsp; &nbsp;
												<label><form:radiobutton  name="isActive" id="isactive" value="2" path="isActive" checked="checked" /> InActive</label>
											</c:if>
										</div>
									</div>
									<div class="clearfix">&nbsp;</div>
									<div class="form-group">
										<label class="col-lg-4 control-label">&nbsp;</label>
										<div class="col-lg-8">
											<a href="cancelRole" class="btn btn-white"><strong>Cancel</strong></a> &nbsp; &nbsp;
											<input type="submit" class="btn btn-primary" id="saveRole" value="Save">
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<jsp:include page="../layout/footer.jsp" />
		</div>
	</div>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>		
	<script type="text/javascript">
	$(document).ready(function(){
		$("#saveRole").click(function(){
			var authority = $("#authority").val();
			if(authority == null || authority =="")
				{$("#authorityErr").text("Authority required");return false;}
			
		});
	});
	</script>
</body>
</html>
