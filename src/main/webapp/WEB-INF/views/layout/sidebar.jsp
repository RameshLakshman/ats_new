<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%   String authority = (String) session.getAttribute("authority");
	String currentUser = (String) session.getAttribute("currenUser");
	if(currentUser != null){
		pageContext.setAttribute("currentUser", currentUser);
	}
%>
<nav class="navbar-default navbar-static-side" role="navigation">
	<div class="sidebar-collapse">
		<ul class="nav" id="side-menu">
			<li class="nav-header">
				<div class="dropdown profile-element">
					<span style="color: white;"><!-- <img alt="image" class="img-circle"
						src="user-design/img/logo.png"> -->Welcome ! ${currentUser} </span> <a href="#"><span
						class="block m-t-xs"> <strong class="font-bold"></strong></span></a>
				</div>
				<div class="logo-element">ATS</div>
			</li>
			<li><a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a></li>
			<li><a href="viewTeam"><i class="fa fa-cog"></i> <span class="nav-label">Team Leaders</span></a></li>
			<li><a href="viewUser"><i class="fa fa-cog"></i> <span class="nav-label">User</span></a></li>
			<li><a href="viewRole"><i class="fa fa-cog"></i> <span class="nav-label">Role</span></a></li>
			<li><a href="clientList"><i class="fa fa-cog"></i> <span class="nav-label">Manage Clients</span></a></li>
			<li><a href="viewTracker"><i class="fa fa-cog"></i> <span class="nav-label">Manage Trackers</span></a></li>
			<li><a href="viewTrackerFormat"><i class="fa fa-cog"></i> <span class="nav-label">Manage Tracker Formats</span></a></li>
			<li><a href="quesList"><i class="fa fa-cog"></i> <span class="nav-label">Manage Questionnaire</span></a></li>


		</ul>
	</div>
</nav>
