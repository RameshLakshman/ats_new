$("document").ready(function(){
	$("#save-tracker").click(function(){
	var existsClient =  $("#existsClient").val().trim();
	var primarySkill = $("#primarySkill").val().trim();
	var additionalSkill = $("#additionalSkill").val().trim();
	var experienceTo = $("#experienceTo").val().trim();
	var experienceFrom = $("#experienceFrom").val().trim();
	var jobLocation = $("#jobLocation").val().trim();
	var salaryFrom =  $("#salaryFrom").val().trim();
	var salaryTo = $("#salaryTo").val().trim();
	
	if(existsClient == null || existsClient == ""){
		$("#trackerErr").html("Please choose Exists client or add new Client");
		return false;
	}
	else if(primarySkill == null || primarySkill == ""){
		$("#trackerErr").html("Please Enter Primary Skill");
		return false;
	}
	else if(jobLocation == null || jobLocation == ""){
		$("#trackerErr").html("Please Enter Job Location");
		return false;
	}
	else if(additionalSkill == null || additionalSkill == ""){
		$("#trackerErr").html("Please Enter Additional Skill");
		return false;
	}
	else if(experienceFrom == 0 && experienceTo == 0 ){
		$("#trackerErr").html("Please Enter Experience Fields");
	}
	else if(experienceFrom == 0 || experienceFrom == ""){
		$("#trackerErr").html("Please Enter Experience From");
		return false;
	}
	else if(experienceTo == 0 || experienceTo == ""){
		$("#trackerErr").html("Please Enter Experience To");
		return false;
	}
	else if(salaryFrom == 0 && salaryTo == 0){
		$("#trackerErr").html("Please Enter Salary Fields");
	}
	else if(salaryFrom == 0 || salaryFrom == ""){
		$("#trackerErr").html("Please Enter Salary From");
		return false;
	}
	else if(salaryTo == 0 || salaryTo == ""){
		$("#trackerErr").html("Please Enter Salary To");
		return false;
	}
	else{
		return true;
	}
	});
	
	$("#submitTrackerFrmt").click(function(){
		var field =  $("#field").val().trim();
		var trackerFk = $("#trackerFk").val().trim();
		if(field == null || field == ""){
			$("#trackerErr").html("Please Enter Tracker Format field");
			return false;
		}
		else if(trackerFk == null || trackerFk == ""){
			$("#trackerErr").html("Please Choose Tracker");
			return false;
		}
		else{
			return true;
		}
		
		return false;
	});
	function isUrlValid(url) {
	    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
	}
	$("#add-new-client").click(function(){
		var clientName =  $("#client_name").val().trim();
		var domain = $("#domain").val().trim();
		var mail_id =  $("#mail_id").val().trim();
		var client_url = $("#client_url").val().trim();
		var spoc =  $("#spoc").val().trim();
		var checkEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if(clientName == null || clientName == ""){
			$("#clientErr").html("Please Enter client Name");
			return false;
		}
		else if(domain == null || domain == ""){
			$("#clientErr").html("Please Enter domain");
			return false;
		}
		if(mail_id == null || mail_id == ""){
			$("#clientErr").html("Please Enter Mail id");
			return false;
		}
		else if(checkEmail.test(mail_id) == false){
			$("#clientErr").html("Please Enter Valid Mail Id");
			return false;
		}
		else if(client_url == null || client_url == ""){
			$("#clientErr").html("Please Enter URL");
			return false;
		}
		else if(!isUrlValid(client_url) ){
			$("#clientErr").html("Please Enter valid URL");
			return false;
		}
		else if(spoc == null || spoc == ""){
			$("#clientErr").html("Please Enter Spoc");
			return false;
		}
		else{
			return true;
		}
		
	});
	$("#submit-questionnaire").click(function(){
		var field =  $("#field").val().trim();
		var trackerFk = $("#trackerFk").val().trim();
		if(field == null || field == ""){
			$("#quesErr").html("Please Enter Questionnaire field");
			return false;
		}
		else if(trackerFk == null || trackerFk == ""){
			$("#quesErr").html("Please Choose Tracker");
			return false;
		}
		else{
			return true;
		}
		
		return false;
	});
	
});