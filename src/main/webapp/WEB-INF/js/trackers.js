$(document).ready(function(){
	
	/* tracker format variables */
	$(".spinner").hide();
	$("#checkFormat").val('');
	var sno="", name="", mail="", mobileNo="", noticePeriod="", ctc="", tfname="", field="";
	var tfnameId="";
	/* end */
	
	/* tracker questionnaire variables */
	$("#checkQuestion").val('');
	var project ="", application="", location="", questionname="", questionfield="";
	var questionNameId;
	/* end */
	
	
	$("#submit-tracker").click(function(){
		 $("#trackerErr").text("");
		var checkformat = $("#checkFormat").val();  /* find new/previous tracker format */
		var checkQuestion = $("#checkQuestion").val();
		var existsClient =  $("#existsClient").val().trim();
		var primarySkill = $("#primarySkill").val().trim();
		var additionalSkill = $("#additionalSkill").val().trim();
		var experienceTo = $("#experienceTo").val().trim();
		var experienceFrom = $("#experienceFrom").val().trim();
		var jobLocation = $("#jobLocation").val().trim();
		var salaryFrom =  $("#salaryFrom").val().trim();
		var salaryTo = $("#salaryTo").val().trim();
		
		if(existsClient == null || existsClient == ""){
			$("#trackerErr").html("Please choose Exists client or add new Client");
			return false;
		}
		else if(primarySkill == null || primarySkill == ""){
			$("#trackerErr").html("Please Enter Primary Skill");
			return false;
		}
		else if(jobLocation == null || jobLocation == ""){
			$("#trackerErr").html("Please Enter Job Location");
			return false;
		}
		else if(additionalSkill == null || additionalSkill == ""){
			$("#trackerErr").html("Please Enter Additional Skill");
			return false;
		}
		else if(experienceFrom == 0 && experienceTo == 0 ){
			$("#trackerErr").html("Please Enter Experience Fields");
		}
		else if(experienceFrom == 0 || experienceFrom == ""){
			$("#trackerErr").html("Please Enter Experience From");
			return false;
		}
		else if(experienceTo == 0 || experienceTo == ""){
			$("#trackerErr").html("Please Enter Experience To");
			return false;
		}
		else if(salaryFrom == 0 && salaryTo == 0){
			$("#trackerErr").html("Please Enter Salary Fields");
		}
		else if(salaryFrom == 0 || salaryFrom == ""){
			$("#trackerErr").html("Please Enter Salary From");
			return false;
		}
		else if(salaryTo == 0 || salaryTo == ""){
			$("#trackerErr").html("Please Enter Salary To");
			return false;
		}
		else if(checkformat == null || checkformat==""){
			$("#trackerErr").text("Tracker Format Required");
			return false;
		}else if(checkQuestion == null || checkQuestion == ""){
			$("#trackerErr").text("Tracker Question Required");
			return false;
		}
		else{
			$(".spinner").show();
		$.ajax({
			type:'POST',
			async: false,
			url:'createTracker',
			data: {
				existsClient: $("#existsClient").val(),
				primarySkill: $("#primarySkill").val(),
				additionalSkill: $("#additionalSkill").val(),
				experienceFrom: $("#experienceFrom").val(),
				experienceTo: $("#experienceTo").val(),
				jobLocation: $("#jobLocation").val(),
				salaryFrom: $("#salaryFrom").val(),
				salaryTo: $("#salaryTo").val()
			},
			success:function(trackerId){
				
				/* new/ previous tracker formats */
				if(checkformat!= null && checkformat!="" && checkformat!="undefined" && checkformat == "newf"){
					var ids = [];
					 var inputs = $('#newFormat :input:checkbox');
					 inputs.each(function(index){ ids.push($(this).val()); });
					 saveNewFormat(trackerId,ids);
					
					
					
				}
				else if(checkformat!= null && checkformat!="" && checkformat!="undefined" && checkformat == "previousf"){
					savePreviousFormat(trackerId);
				}
				
				/*  new/ previous questions*/
				if(checkQuestion!= null && checkQuestion!="" && checkQuestion!="undefined" && checkQuestion == "newq"){
					
					 var qusid = [];
					 var qusinput = $('#newQuestion :input:checkbox');
					 qusinput.each(function(index){ qusid.push($(this).val()); });
				     saveNewQuestions(trackerId, qusid);
				     
				}else if(checkQuestion!= null && checkQuestion!="" && checkQuestion!="undefined" && checkQuestion == "previousq"){
				    savePrevQuestions(trackerId);
				}
				 $('#trackerModal').modal('toggle');
				 window.location.href="";
				
			}
		});	
		}
		return false;
	});
	
	function isUrlValid(url) {
	    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
	}
	
	$("#submit-client").click(function(){
		$("#clientErr").html("");
		
		var newClient =  $("#clientName").val().trim();
		var clientLocation = $("#location").val().trim();
		var domain = $("#domain").val().trim();
		var url = $("#url").val().trim();
		var spoc = $("#spoc").val().trim();
		var checkEmail = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		/*$('#existsClient').find('option').remove().end()
		var parentValueToPush = [];
		 $('#existsLocation :selected').each(function() {
			 parentValueToPush.push($(this).val());

	        });
		 alert(parentValueToPush)*/
		if(newClient == null || newClient == ""){
			$("#clientErr").html("Please Enter Client Name");
			return false;
		}
		else if(clientLocation == null || clientLocation == ""){
			$("#clientErr").html("Please Enter Location");
			return false;
		}
		else if(domain == null || domain == ""){
			$("#clientErr").html("Please Enter Domain");
			return false;
		}
		else if(url == null || url == ""){
			$("#clientErr").html("Please Enter URL");
			return false;
		}
		else if(!isUrlValid(url) ){
			$("#clientErr").html("Please Enter valid URL");
			return false;
		}
		else if(spoc == null || spoc == "" ){
			$("#clientErr").html("Please Enter Spoc Mail Id");
		}
		else if(checkEmail.test(spoc) == false){
			$("#clientErr").html("Please Enter Valid Mail Id");
		}
		else{
		$.ajax({
			type:'POST',
			async: false,
			url:'addNewClient',
			data: {
				clientName : newClient,
				domain : domain,
				spoc : spoc,
				url: url,
				existsLocation : clientLocation
			},
			success:function(data){
				$('#existsClient').find('option').remove().end()
				$.each(data, function(index, value) {
       			$.each(value, function(i, val){
       				if(index == val.client_id){
       					$('#existsClient').append($('<option selected="selected">').text(val.client_name).val(val.client_id));
       				}
       				if(index != val.client_id){
       				$('#existsClient').append($('<option>').text(val.client_name).val(val.client_id));
       				}
       			});      			  
       			 
       		    });
				 $('#addClient').modal('toggle');
			}
		});	
		}
		return false;
	});
	
	$("#searchClient").click(function(){
		var client = $("#searchclient").val().trim();
	     var expFrom = $("#expFrom").val().trim();
		 var expTo = $("#expTo").val().trim();
		 var skill = $("#skill").val().trim();
		 
		    $("#clientEr").html("");
			$("#skillErr").html("");
			
			if(client=="" || client==null){
				$("#clientEr").html("Client Name is required.");
				return false;
			}		
			else if(expFrom == 0 && expTo == 0 ){
				$("#expErr").html("Please Enter Experience Fields");
			}
			else if(expFrom == 0 || expFrom == ""){
				$("#expErr").html("Please Enter Experience From");
				return false;
			}
			else if(expTo == 0 || expTo == ""){
				$("#expErr").html("Please Enter Experience To");
				return false;
			}
			else if(skill == "" || skill == null){
				$("#skillErr").html("Skill is required.");
				return false;
			}
			else{
			 $.ajax({
			        	type: 'POST',
			        	async: false,
			        	url: 'searchTracker',
			        	data: {
			        		client : $("#searchclient").val(),
			        		expFrom : $("#expFrom").val(),
			        		expTo : $("#expTo").val(),
			        		skill : $("#skill").val()
			        	},
			        	success: function(data){
			        		
			        		$('#allListvalues').css('display', 'none');
			        		$('#searchErrors').css('display', 'block');
			        		$.each(data, function(index, value) {
			           			$.each(value, function(i, val){
			           				
			           			 if(val!=null){
			           				$('#ajaxSearchResponse').css('display', 'block');
			           				$('#access_link_search').css('display', 'none');
			           				$('#searchErrors').css('display', 'none');
			           				
			           			 	clas = "boxed col-md-3 col-sm-6";
			           			     $("#ajaxSearchResponse").append("<div class ='"+clas+"'><tr><td>" + val.clientFk.client_name + ","+ val.location + ""+ "<br>"
			           			     + val.primarySkills + "<br>"+ val.experienceFrom + "-"+ val.experienceTo + " Years <br>"
			           			     + val.salaryFrom + "-"+ val.salaryTo + " LPA <br></td></tr></div>");			           			 
			                        
			                     } 
			                    
			                     else {
			                    	 $('#ajaxSearchResponse').css('display', 'none');
			                    	 $("#searchErrors").html("No results found.");
			                 
			                     }
			           			});      			  
			           			 
			           		    });
			    				 $('#searchTracker').modal('toggle');
			        	
			            },
			            error:function(){
			            	alert("error")
			            }
			        	});
			}
				 return false;	 
	    });
	 
	$("#savenewFormat").click(function(){
		$("#checkFormat").val('newf');
		tfname = $("#tfname").val();
		if(tfname != null && tfname !="")
		{
		 sno = $("#sno").is(":checked")?$("#sno").val(): "";
		 name =  $("#name").is(":checked")?$("#name").val():"";
		 mail =  $("#mailId").is(":checked")?$("#mailId").val():"";
		 mobileNo =  $("#mobileNo").is(":checked")?$("#mobileNo").val():"";
		 noticePeriod =  $("#noticePeriod").is(":checked")?$("#noticePeriod").val():"";
		 ctc =  $("#ctc").is(":checked")?$("#ctc").val():"";
		 
		  if(sno == "" || sno == null){
			  $("#tfErr").text("S. No Required"); return false;
		  }     
		  if(name == "" || name == null){
			  $("#tfErr").text("Name Required"); return false;
		  }    
		  if(mail == "" || mail == null){
			  $("#tfErr").text("Mail Required"); return false;
		  }    
		  if(mobileNo == "" || mobileNo == null){
			  $("#tfErr").text("Mobile Number Required"); return false;
		  }    
		  if(noticePeriod == "" || noticePeriod == null){
			  $("#tfErr").text("Notice Period Required"); return false;
		  }    
		  if(ctc == "" || ctc == null){
			  $("#tfErr").text("CTC Required"); return false;
		  }    
		 
		$("#newFormat").modal('toggle');
	 }else{
		 $("#tfErr").text("Tracker Format Name Required");
		 return false;
	 }
		
	}); 
	
	$("#prevformat_save").click(function(){
		$("#checkFormat").val('previousf');
		$("#prevformatErr").text("");
		 tfnameId = "";
		if($(".prevformat_radio").length != 0){
			if($(".prevformat_radio").is(":checked")){
				tfnameId = $(".prevformat_radio:checked").val();
				$("#prevFormat").modal('toggle');
			}else  $("#prevformatErr").text("Please Choose Format");
		}else $("#prevFormat").modal('toggle');
		
	});
	$("#savenewQuestion").click(function(){
		
		$("#checkQuestion").val("newq");
		questionname = $("#questionName").val();
		if(questionname != null && questionname !="")
		{
		 project = $("#project").is(":checked")?$("#project").val(): "";
		 application =  $("#application").is(":checked")?$("#application").val():"";
		 location =  $("#locations").is(":checked")?$("#locations").val():"";
		 
		  if(project == "" || project == null){
			  $("#questionErr").text("project Required"); return false;
		  }     
		  if(application == "" || application == null){
			  $("#questionErr").text("application Required"); return false;
		  }    
		  if(location == "" || location == null){
			  $("#questionErr").text("location Required"); return false;
		  }    
		$("#newQuestion").modal('toggle');
	 }else{
		 $("#questionErr").text("Questionnaire Name Required");
		 return false;
	 }
	});
	
	$("#prevquestions_save").click(function(){
		$("#checkQuestion").val("previousq");
		$("#prevquestionErr").text("");
		questionNameId = "";
		if($(".prevquestion_radio").length != 0){
			if($(".prevquestion_radio").is(":checked")){
				questionNameId = $(".prevquestion_radio:checked").val();
				$("#prevQuestion").modal('toggle');
			}else  $("#prevquestionErr").text("Please Choose Format");
		}else $("#prevQuestion").modal('toggle');
		
	});

	$("#addExtraField").click(function(){
		  field = $("#trackerField").val();
		  var removspace = field.replace(/\s/g,'');
		  if(field ==null || field =="") $("#atfErr").text("Tracker Format Required");
		  else {
		   var classname = "'form-group "+removspace+"'";
		   var id="."+removspace;
		   $("div#addextra").before("<div class="+classname+"></div>")
		   $(id).append("<div class='col-sm-10'><label class='control-label col-sm-8'>"+field+"</label><input type='checkbox' id="+removspace+" value="+field+" checked='checked'>");
		   $("#addNewFormat").modal("toggle");
		  }
		  
		  return false;
		 });
	
	
	$("#addCusQus ").click(function(){
		questionfield = $("#questionField").val();
		if(questionfield ==null || questionfield =="") $("#addquestionErr").text("Question Required");
		else {
			var removeExcla = questionfield.replace("?","");
			var removspace = removeExcla.replace(/\s/g,'');
			var classname = "'form-group "+removspace+"'";
			var id="."+removspace;
			$("div#cusQus").before("<div class="+classname+" id='clear'></div>")
			$("div#clear").before("<div class='clearfix'>&nbsp;</div>")
			$(id).append("<div class='col-sm-12'><label class='control-label col-sm-10'>"+questionfield+"</label><input type='checkbox' id="+removspace+" value="+questionfield+" checked='checked'>");
			$("#addNewQuestion").modal("toggle");
			}
		
		
		
		
		return false;
	});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
function saveNewFormat(trackerId, fields){
	$.ajax({
    	type: 'POST',
    	async: false,
    	url: "newTrackerFormat",
    	data: {
    		trackerId: trackerId,
    		tfname: tfname,
			field: fields
    	},
    	success: function(result){
    			if(result != "true"){
    				$("#tfErr").text(result);
    			}
        }
   	 }); 
}
function savePreviousFormat(trackerId){
	if(tfnameId !=null && tfnameId!=""){
		$.ajax({
			type: 'POST',
    		async: false,
    		url: "saveprevformat",
    		data: {
    			trackerId: trackerId,
    			tfnameId: tfnameId
    		},
    		success: function(result){
    			if(result !="true") response = false;
    			else response = true;
        	}
		});
	}else  $("#prevformatErr").text("Sorry! Request failed");
}
function saveNewQuestions(trackerId, fields){
	$.ajax({
    	type: 'POST',
    	async: false,
    	url: "savenewques",
    	data: {
    		trackerId: trackerId,
    		questionname: questionname,
			field: fields
    	},
    	success: function(result){
    		if(result != "true"){
				$("#questionErr").text(result);
			}
        }
   	 }); 
}
function PrevQuestions(trackerId){
	if(questionNameId !=null && questionNameId!=""){
		$.ajax({
			type: 'POST',
    		async: false,
    		url: "saveprevquestions",
    		data: {
    			trackerId: trackerId,
    			questionNameId: questionNameId
    		},
    		success: function(result){
    			if(result !="true") response = false;
    			else response = true;
        	}
		});
	}else  $("#prevquestionErr").text("Sorry! Request failed");
}

$("#addTracker").click(function(){
$("input, select, textarea")
.not("input[type=button], input[type=submit],input[type=number],input[type=checkbox],input[type=radio]")
.val("");
$("input[type=number]").val(0);
});

$("#newClient").click(function(){
	 $('#addClient').find('input:text').val('');    
});

});
	    function validateQty(event) {
		    var key = window.event ? event.keyCode : event.which;

		if (event.keyCode == 8 || event.keyCode == 46
		 || event.keyCode == 37 || event.keyCode == 39) {
		    return true;
		}
		else if ( key < 48 || key > 57 ) {
		    return false;
		}
		else return true;
		};
